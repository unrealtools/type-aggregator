#pragma once
#include <magic_enum.hpp>

using namespace magic_enum;

template <typename E>
[[nodiscard]] auto enum_flags_names(E value) -> detail::enable_if_t<E, std::vector<string>> {
	using D = std::decay_t<E>;
	using U = underlying_type_t<D>;
	constexpr auto S = detail::enum_subtype::flags;

	std::vector<string> names;
	auto check_value = U{ 0 };
	for (std::size_t i = 0; i < detail::count_v<D, S>; ++i) {
		if (const auto v = static_cast<U>(enum_value<D, S>(i)); (static_cast<U>(value) & v) != 0) {
			check_value |= v;
			string n = std::string(detail::names_v<D, S>[i]);
			names.push_back(n);
		}
	}

	if (check_value != 0 && check_value == static_cast<U>(value)) {
		return names;
	}
	return {}; // Invalid value or out of range.
}