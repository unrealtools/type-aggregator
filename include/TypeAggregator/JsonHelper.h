#pragma once

#include <nlohmann/json.hpp>

#define NLOHMANN_DERIVED_TYPE_TO_JSON(Type, Base, ...) \
    void to_json(nlohmann::json& nlohmann_json_j, const Type& nlohmann_json_t) { nlohmann::to_json(nlohmann_json_j, static_cast<const Base>(nlohmann_json_t)); NLOHMANN_JSON_EXPAND(NLOHMANN_JSON_PASTE(NLOHMANN_JSON_TO, __VA_ARGS__)) }


#define NLOHMANN_DEFINE_DERIVED_TYPE_INTRUSIVE(Type, Base, ...) \
    friend NLOHMANN_DERIVED_TYPE_TO_JSON(Type, Base, __VA_ARGS__) \
    friend void from_json(const nlohmann::json& nlohmann_json_j, Type& nlohmann_json_t) { nlohmann::from_json(nlohmann_json_j, static_cast<Base&>(nlohmann_json_t)); NLOHMANN_JSON_EXPAND(NLOHMANN_JSON_PASTE(NLOHMANN_JSON_FROM, __VA_ARGS__)) }

#define NLOHMANN_DEFINE_DERVIED_TYPE_INTRUSIVE_WITH_DEFAULT(Type, Base, ...) \
    friend NLOHMANN_DERIVED_TYPE_TO_JSON(Type, Base, ...) \
    friend void from_json(const nlohmann::json& nlohmann_json_j, Type& nlohmann_json_t) { nlohmann::from_json(nlohmann_json_j, static_cast<Base&>(nlohmann_json_t)); Type nlohmann_json_default_obj; NLOHMANN_JSON_EXPAND(NLOHMANN_JSON_PASTE(NLOHMANN_JSON_FROM_WITH_DEFAULT, __VA_ARGS__)) }

#define NLOHMANN_DEFINE_DERIVED_TYPE_NON_INTRUSIVE(Type, Base, ...) \
    inline NLOHMANN_DERIVED_TYPE_TO_JSON(Type, Base, __VA_ARGS__) \
    inline void from_json(const nlohmann::json& nlohmann_json_j, Type& nlohmann_json_t) { nlohmann::from_json(nlohmann_json_j, static_cast<Base&>(nlohmann_json_t)); NLOHMANN_JSON_EXPAND(NLOHMANN_JSON_PASTE(NLOHMANN_JSON_FROM, __VA_ARGS__)) }

#define NLOHMANN_DEFINE_DERIVED_TYPE_NON_INTRUSIVE_WITH_DEFAULT(Type, Base, ...) \
    inline NLOHMANN_DERIVED_TYPE_TO_JSON(Type, Base, __VA_ARGS__) \
    inline void from_json(const nlohmann::json& nlohmann_json_j, Type& nlohmann_json_t) { nlohmann::from_json(nlohmann_json_j, static_cast<Base&>(nlohmann_json_t)); Type nlohmann_json_default_obj; NLOHMANN_JSON_EXPAND(NLOHMANN_JSON_PASTE(NLOHMANN_JSON_FROM_WITH_DEFAULT, __VA_ARGS__)) }
	