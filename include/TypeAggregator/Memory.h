#pragma once
#define NOMINMAX
#include <Windows.h>
#include <vector>
#include <string>

namespace TypeAggregator {
    class Memory {
    public:
        static Memory* m_Memory;

        struct ProtectedArea {
            uintptr_t start;
            uintptr_t end;
            size_t size;
        };

        class ModuleInfo {
        public:
            uintptr_t modBase;
            size_t modSize;
            std::vector<ProtectedArea> protectedAreas;
        };

    public:
        static Memory* instance();
        static bool isPageReadable(MEMORY_BASIC_INFORMATION* mbi);
        bool getModuleInfo(char* pModuleName, ModuleInfo* moduleInfo);
        bool getModuleInfo(void* pOffset, ModuleInfo* moduleInfo);
        bool getModuleInfoOfAllModules(std::vector<Memory::ModuleInfo>* moduleInfos);
        uintptr_t findPattern(ModuleInfo* moduleInfo, BYTE* pattern, char* mask, size_t startOffset = NULL);
        uintptr_t findPattern(ModuleInfo* moduleInfo, char* pattern, char* mask, size_t startOffset = NULL);
        uintptr_t findPattern(std::vector<ModuleInfo>* moduleInfos, char* pattern, char* mask, size_t startOffset = NULL);

        template<typename TVal>
        uintptr_t findValue(std::vector<ModuleInfo>* moduleInfos, TVal val, size_t startOffset = 0)
        {
            for (auto& modInfo : *moduleInfos)
            {
                if (startOffset > modInfo.modBase + modInfo.modSize)
                    continue;

                uintptr_t ret = findValue(&modInfo, val, startOffset > modInfo.modBase ? startOffset : modInfo.modBase);
                if (ret != 0)
                    return ret;
            }

            return 0;
        }

        template<typename TVal>
        uintptr_t findValue(ModuleInfo* mInfo, TVal val, size_t startOffset = 0)
        {
            uintptr_t start = startOffset != 0 ? startOffset : mInfo->modBase;

            SYSTEM_INFO si;
            GetSystemInfo(&si);

            MEMORY_BASIC_INFORMATION mbi;
            if (!VirtualQuery((LPVOID)start, &mbi, sizeof(mbi)))
                return 0;

            if (mbi.State != MEM_COMMIT || !isPageReadable(&mbi))
                return 0;

            uintptr_t currRegionEnd = (uintptr_t)mbi.BaseAddress + mbi.RegionSize;
            for (int i = startOffset != 0 ? startOffset : mInfo->modBase; i < (mInfo->modBase + mInfo->modSize - sizeof(TVal)); i++)
            {
                if (i + sizeof(TVal) >= currRegionEnd)
                {
                    do 
                    {
                        i = (uintptr_t)mbi.BaseAddress + mbi.RegionSize;
                        if (!VirtualQuery((LPVOID)i, &mbi, sizeof(mbi)))
                            return 0;
                    } while (mbi.State != MEM_COMMIT || !isPageReadable(&mbi));

                    currRegionEnd = (uintptr_t)mbi.BaseAddress + mbi.RegionSize;
                }

                if (*(TVal*)(i) == val)
                    return i;
            }

            return 0;
        }

        template<typename TVal, std::size_t size>
        uintptr_t findValues(std::vector<ModuleInfo>* moduleInfos, const TVal(&values)[size], size_t startOffset = 0, size_t padding = 0)
        {
            uintptr_t firstValueOffset = 0;

            do
            {
                firstValueOffset = findValue(moduleInfos, values[0], startOffset);
                if (firstValueOffset == 0)
                    return 0;

                bool found = true;
                for (int i = 1; i < size - 1; i++)
                {
                    if (*(TVal*)(firstValueOffset + (i * sizeof(TVal))) != values[i])
                    {
                        found = false;
                        break;
                    }
                }

                if (found)
                    return firstValueOffset;
                
                startOffset = firstValueOffset + sizeof(TVal);
            } while (firstValueOffset != 0);

            return 0;
        }
    };
}