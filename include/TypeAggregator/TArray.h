#pragma once

#include <cstdint>
#include <cstdlib>
#include <cstring>
#include <stdexcept>
#include <spdlog/fmt/fmt.h>
#include <TypeAggregator/Types/UnrealType.h>
#include <stdlib.h>

namespace TypeAggregator
{
	template<typename TArray>
	class TIterator
	{
	public:
		using ElementType = typename TArray::ElementType;
		using ElementPointer = ElementType*;
		using ElementReference = ElementType&;
		using ElementConstReference = const ElementType&;

	private:
		ElementPointer IteratorData;

	public:
		TIterator(ElementPointer inElementPointer) : IteratorData(inElementPointer) {}
		~TIterator() {}

	public:
		TIterator& operator++()
		{
			IteratorData++;
			return *this;
		}

		TIterator operator++(int32_t)
		{
			TIterator iteratorCopy = *this;
			++(*this);
			return iteratorCopy;
		}

		TIterator& operator--()
		{
			IteratorData--;
			return *this;
		}

		TIterator operator--(int32_t)
		{
			TIterator iteratorCopy = *this;
			--(*this);
			return iteratorCopy;
		}

		ElementReference operator[](int32_t index)
		{
			return *(IteratorData[index]);
		}

		ElementPointer operator->()
		{
			return IteratorData;
		}

		ElementReference operator*()
		{
			return *IteratorData;
		}

	public:
		bool operator==(const TIterator& other) const
		{
			return (IteratorData == other.IteratorData);
		}

		bool operator!=(const TIterator& other) const
		{
			return !(*this == other);
		}
	};

	template<typename InElementType, typename InSizeType = size_t>
	class TArray
	{
	public:
		using ElementType = InElementType;
		using ElementPointer = InElementType*;
		using ElementReference = InElementType&;
		using ElementConstPointer = const InElementType*;
		using ElementConstReference = const InElementType&;
		using SizeType = InSizeType;
		using Iterator = TIterator<TArray<InElementType, InSizeType>>;

	protected:
		ElementPointer Data;
		SizeType ArrayCount;
		SizeType ArrayMax;

	public:
		TArray() : Data(nullptr), ArrayCount(0), ArrayMax(0) {}
		
		TArray(SizeType max) : ArrayCount(0), ArrayMax(max)
		{
			Data = (ElementPointer)std::calloc(ArrayMax, sizeof(ElementType));
		}

		TArray(std::initializer_list<InElementType> list) : ArrayCount(0), ArrayMax(list.size())
		{
			Data = (ElementPointer)std::malloc(sizeof(ElementType) * list.size());

			for (auto el : list)
				Push(el);
		}

		template<SizeType N>
		TArray(ElementType(&list)[N], SizeType Max = N) : Data(list), ArrayCount(N), ArrayMax(Max)
		{
		}

		~TArray()
		{
			//clear();
			//::operator delete(ArrayData, ArrayMax * sizeof(ElementType));
		}

	public:
		ElementConstReference operator[](SizeType index) const
		{
			return at(index);
		}

		ElementReference operator[](SizeType index)
		{
			return at(index);
		}

		ElementConstReference at(SizeType index) const
		{
			CheckIndexBounds(index);
			return Data[index];
		}

		ElementReference at(SizeType index)
		{
			CheckIndexBounds(index);
			return Data[index];
		}

		ElementConstPointer GetData()
		{
			return Data;
		}

		void Push(ElementConstReference newElement)
		{
			if (ArrayCount >= ArrayMax)
				ReAllocate(sizeof(ElementType) * (ArrayCount + 1));

			new(&Data[ArrayCount]) ElementType(newElement);
			ArrayCount++;
		}

		void Push(ElementReference& newElement)
		{
			if (ArrayCount >= ArrayMax)
				ReAllocate(sizeof(ElementType) * (ArrayCount + 1));

			new(&Data[ArrayCount]) ElementType(newElement);
			ArrayCount++;
		}

		void Pop()
		{
			if (ArrayCount > 0)
			{
				ArrayCount--;
				Data[ArrayCount].~ElementType();
			}
		}

		void Clear()
		{
			for (SizeType i = 0; i < ArrayCount; i++)
			{
				Data[i].~ElementType();
			}

			ArrayCount = 0;
		}

		TArray<ElementType, InSizeType> operator+(TArray<ElementType, InSizeType>& second)
		{
			// TODO: rewrite copy mechanism
			TArray<ElementType, InSizeType> copy;
			for (auto e : *this)
				copy.Push(e);

			for (auto e : second)
				copy.Push(e);

			return copy;
		}

		bool operator==(TArray<ElementType, InSizeType> sec)
		{
			if (ArrayCount != sec.Count())
				return false;

			for (SizeType i = 0; i < sec.Count(); i++)
				if (this->at(i) != sec.at(i))
					return false;

			return true;
		}

		bool operator!=(TArray<ElementType, InSizeType> sec)
		{
			return !(*this == sec);
		}

		Iterator begin()
		{
			return Iterator(Data);
		}

		Iterator end()
		{
			return Iterator(Data + ArrayCount);
		}

		SizeType Count() const
		{
			return ArrayCount;
		}

		SizeType Max() const
		{
			return ArrayMax;
		}

		bool Empty() const
		{
			return Count() == 0;
		}

	private:
		void ReAllocate(SizeType newArrayMax)
		{
			ElementPointer newArrayData = (ElementPointer)::operator new(newArrayMax * sizeof(ElementType));
			SizeType newNum = ArrayCount;

			if (newArrayMax < newNum)
			{
				newNum = newArrayMax;
			}

			for (SizeType i = 0; i < newNum; i++)
			{
				new(newArrayData + i) ElementType(std::move(Data[i]));
			}

			for (SizeType i = 0; i < ArrayCount; i++)
			{
				Data[i].~ElementType();
			}

			::operator delete(Data, ArrayMax * sizeof(ElementType));
			Data = newArrayData;
			ArrayMax = newArrayMax;
		}

	protected:
		void CheckIndexBounds(SizeType index)
		{
			if (index < 0 || index >= Count())
				throw std::out_of_range(fmt::format("Index out of bounds ({} of {})", index, Count()));
		}
	};
}