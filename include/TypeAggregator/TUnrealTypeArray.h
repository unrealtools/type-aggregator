#pragma once

#include <TypeAggregator/Types/UnrealType.h>
#include <TypeAggregator/TypeDef/Registry.h>
#include <TypeAggregator/TArray.h>

namespace TypeAggregator
{
	//using namespace ::TypeAggregator::Types;

	template<typename InElementType, typename InSizeType = size_t>
		requires(std::is_base_of_v<Types::UnrealType, InElementType> || std::is_base_of_v<Types::UnrealType, std::remove_pointer_t<InElementType>>)
	class TUnrealTypeArray
	{
	protected:
		using ElementType = std::remove_pointer_t<InElementType>;
		using ElementPointer = ElementType*;
		using ElementReference = ElementType&;
		using ElementConstPointer = const ElementType*;
		using ElementConstReference = const ElementType&;
		using SizeType = InSizeType;
		using Iterator = TIterator<TArray<ElementType, InSizeType>>;
		using ArrayType = TArray<uintptr_t, SizeType>;

		ArrayType* Data;
		ElementPointer* InstanceData;

	public:
		TUnrealTypeArray() : Data(nullptr), InstanceData(nullptr) {}

		TUnrealTypeArray(uintptr_t InArrayPointer) :
			Data((ArrayType*)InArrayPointer), InstanceData(nullptr)
		{
			InstanceData = (ElementPointer*)std::calloc(Data->Max(), sizeof(ElementPointer));
		}

		SizeType Count()
		{
			return Data->Count();
		}

		SizeType Max()
		{
			return Data->Max();
		}

		ElementPointer at(SizeType index)
		{
			CheckIndexBounds(index);

			if (InstanceData[index] == nullptr)
			{
				if constexpr (std::is_pointer_v<InElementType>)
				{
					InstanceData[index] = new ElementType((uintptr_t)(Data->at(index)));
				}
				else
				{
					static auto typeDef = TypeDef::TypeRegistry::GetTypeDef<ElementType>();
					InstanceData[index] = new ElementType((uintptr_t)Data->GetData() + (index * typeDef->Size));
				}

			}

			return InstanceData[index];
		}

		ElementPointer operator[](SizeType index)
		{
			return at(index);
		}

	protected:
		void CheckIndexBounds(SizeType index)
		{
			if(index < 0 || index >= Count())
				throw std::out_of_range(fmt::format("Index out of bounds ({} of {})", index, Count()));
		}
	};
}