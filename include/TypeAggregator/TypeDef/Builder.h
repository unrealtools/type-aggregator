#pragma once
#include <TypeAggregator/UnrealScanner.h>

namespace TypeAggregator::TypeDef
{
	class TypeBuilder
	{
	protected:
		UnrealScanner* Scanner;
	public:
		TypeBuilder(UnrealScanner* scanner);

	public:
		void BuildAll();

		void BuildNameDef();
		void BuildNameEntryDef();
		void BuildStringDef();
		void BuildArrayDef();

		void BuildScriptDelegateDef();
		void BuildFrameDef();

		void BuildObjectDef();
		void BuildFieldDef();
		void BuildStructDef();
		void BuildClassDef();
		void BuildPropertyDef();
		void BuildConstDef();
		void BuildEnumDef();
		void BuildFunctionDef();

		void BuildObjectPropertyDef();
		void BuildClassPropertyDef();
		void BuildInterfacePropertyDef();
		void BuildArrayPropertyDef();
		void BuildMapPropertyDef();
		void BuildStructPropertyDef();
		void BuildBytePropertyDef();
	};
}