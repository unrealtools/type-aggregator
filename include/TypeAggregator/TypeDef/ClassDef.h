#pragma once

#include <TypeAggregator/TypeDef/StructDef.h>
#include <TypeAggregator/TypeDef/FunctionDef.h>

namespace TypeAggregator::Types::Core
{
	class UClass;
}

namespace TypeAggregator::TypeDef
{
	struct ClassDef : public StructDef
	{
		std::vector<std::string> ClassFlags = {};
		int DefaultClassIndex = -1;
		int VTableAddress = 0;

		ClassDef() = default;
		ClassDef(TypeAggregator::Types::Core::UClass* cls);

		std::map<std::string, std::string> Constants = std::map<std::string, std::string>();
		std::map<std::string, std::vector<std::string>> Enums = std::map<std::string, std::vector<std::string>>();
		std::vector<std::string> States = std::vector<std::string>();
		std::vector<FunctionDef> Functions = std::vector<FunctionDef>();

		FunctionDef* GetFunctionByName(std::string functionName);
	};

	NLOHMANN_DEFINE_DERIVED_TYPE_NON_INTRUSIVE_WITH_DEFAULT(ClassDef, StructDef,
		ClassFlags, DefaultClassIndex, VTableAddress, Constants, Enums, States, Functions);
}