#pragma once
#include <string>
#include <vector>

#include <TypeAggregator/TypeDef/ObjectDef.h>
#include <TypeAggregator/TypeDef/PropertyDef.h>

namespace TypeAggregator::Types::Core
{
	class UFunction;
}

namespace TypeAggregator::TypeDef
{
	struct FunctionDef : public ObjectDef
	{
		static const uintptr_t ProcessEventAddress;

		uintptr_t Pointer;
		std::vector<std::string> FunctionFlags = {};
		std::string ReturnType;

		int NativeIndex = 0;

		uint32_t NumParams = 0;
		uint32_t ParamSize = 0;
		uint32_t RepOffset = std::numeric_limits<uint16_t>::min();
		uint32_t ReturnValueOffset = std::numeric_limits<uint16_t>::max();

		std::vector<PropertyDef> Parameters = std::vector<PropertyDef>();
		std::vector<PropertyDef> Locals = std::vector<PropertyDef>();
		std::string Script = "";

		FunctionDef() = default;
		FunctionDef(TypeAggregator::Types::Core::UFunction* func);

		PropertyDef* GetParameterByName(std::string propertyName);
		PropertyDef* GetParameterByOffset(uint32_t offset);

		PropertyDef* GetLocalByName(std::string localName);
		PropertyDef* GetLocalByOffset(uint32_t offset);
	};

	NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE_WITH_DEFAULT(FunctionDef, Name,
		NativeIndex, Pointer, NumParams, ParamSize, RepOffset, ReturnValueOffset, ReturnType, Parameters, Locals, Script, FunctionFlags);
}