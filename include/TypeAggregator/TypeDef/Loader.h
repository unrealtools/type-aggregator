#pragma once

#include <string>
#include <nlohmann/json.hpp>
#include <filesystem>

namespace TypeAggregator::TypeDef
{
	struct StructDef;

	class TypeDefLoader
	{
	public:
		TypeDefLoader(std::string typeDefinitionFolder)
			: DefinitionFolder(typeDefinitionFolder) {}

		StructDef* LoadDefinitionFile(std::string typeSlug);
		std::vector<StructDef*> LoadDefinitionFiles(std::initializer_list<std::string> typeSlugs);

	protected:
		std::string DefinitionFolder;

		nlohmann::json ParseJsonFile(std::string filePath);
		nlohmann::json ParseJsonFile(std::filesystem::path filePath)
		{
			return ParseJsonFile(filePath.string());
		}
	};
}