#pragma once
#include <string>
#include <TypeAggregator/JsonHelper.h>

namespace TypeAggregator::TypeDef
{
	struct ObjectDef
	{
		std::string Name;
		std::string ParentName = "";
		std::string PackageName;
		int ObjectInternalInteger = -1;

		std::vector<std::string> ObjectFlags = {};

		ObjectDef() : Name("") {}
	};

	NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE_WITH_DEFAULT(ObjectDef,
		Name, ParentName, PackageName, ObjectInternalInteger, ObjectFlags);
}