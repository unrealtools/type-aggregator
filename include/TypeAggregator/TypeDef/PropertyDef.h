#pragma once

#include <string>
#include <vector>
#include <TypeAggregator/TypeDef/ObjectDef.h>

namespace TypeAggregator::Types::Core
{
	class UProperty;
}

namespace TypeAggregator::TypeDef
{
	struct PropertyDef : ObjectDef
	{
		std::string Type;
		uint32_t Offset;
		std::vector<std::string> PropertyFlags = {};
		int PropertySize;

		PropertyDef() = default;
		PropertyDef(TypeAggregator::Types::Core::UProperty* prop);
		PropertyDef(std::string name, uint32_t offset, std::string type, int propertySize) :
			ObjectDef(), Offset(offset), Type(type), PropertySize(propertySize)
		{
			Name = name;
		}

	};

	NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE_WITH_DEFAULT(PropertyDef, Name,
		Type, Offset, PropertyFlags, PropertySize);
}