#pragma once

#include <string>
#include <typeindex>
#include <typeinfo>
#include <unordered_map>

#include <TypeAggregator/TypeDef/Loader.h>

namespace TypeAggregator::TypeDef
{
	struct StructDef;

	class TypeRegistry
	{
	public:
		static void Initialize(TypeDefLoader* typeDefLoader);

		template<class T>
		static StructDef* GetTypeDef()
		{
			return GetTypeDef(T::ClassName);
		}

		static StructDef* GetTypeDef(std::string typeName);

		static StructDef* GetTypeDef(int objectInternalInteger);

		static std::vector<StructDef*> GetTypeDefs();

		template<class T>
		static void AddTypeDef(StructDef* def)
		{
			AddTypeDef(T::ClassName, def);
		}

		static void AddTypeDef(std::string typeName, StructDef* def);

	protected:
		inline static std::map<std::string, StructDef*> StructDefinitions = std::map<std::string, StructDef*>();
		inline static std::map<std::type_index, StructDef*> StructDefClassMap = std::map<std::type_index, StructDef*>();

		inline static TypeDefLoader* Loader = nullptr;

		static std::map<std::type_index, std::string> RequiredTypeDefinitions;

	private:
		TypeRegistry();
	};
}
