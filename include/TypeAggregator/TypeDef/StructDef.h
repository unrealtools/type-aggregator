#pragma once

#include <string>
#include <vector>
#include <map>

#include <TypeAggregator/TypeDef/ObjectDef.h>
#include <TypeAggregator/TypeDef/PropertyDef.h>

namespace TypeAggregator::Types::Core
{
	class UStruct;
}

namespace TypeAggregator::TypeDef
{
	struct StructDef : ObjectDef
	{
		std::vector<PropertyDef> Properties = std::vector<PropertyDef>();
		int Size = 0;
		int StructSize = 0;

		StructDef() = default;

		StructDef(TypeAggregator::Types::Core::UStruct* str);

		StructDef(std::string name, std::string package, std::string parent, int objectInternalInteger)
			: ObjectDef()
		{
			Name = name;
			PackageName = package;
			ParentName = parent;
			ObjectInternalInteger = objectInternalInteger;
		}

		PropertyDef* GetPropertyByName(std::string propertyName);
		PropertyDef* GetPropertyByOffset(uint32_t offset);

		bool HasProperty(std::string propertyName);
		bool HasProperty(uint32_t offset);
	};

	NLOHMANN_DEFINE_DERIVED_TYPE_NON_INTRUSIVE_WITH_DEFAULT(StructDef, ObjectDef,
		Properties, Size, StructSize);
}