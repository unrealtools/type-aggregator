#pragma once

#include <nlohmann/json.hpp>

#include <TypeAggregator/Types/Core/UObject.h>
#include <TypeAggregator/Types/Core/UnrealNames.h>
#include <TypeAggregator/Types/Core/UStruct.h>
#include <TypeAggregator/Types/Core/UClass.h>


namespace TypeAggregator
{
	class Serializer
	{
	public:
		void SerializeStruct(nlohmann::json& j, Types::Core::UStruct* str);
		void SerializeClass(nlohmann::json& j, Types::Core::UClass* cls);
	};
}