#pragma once

namespace TypeAggregator::Types::Core
{
	enum class EClassFlags : uint32_t
	{
		// Base flags.
		None = 0x00000000,
		Abstract = 0x00000001,	// Class is abstract and can't be instantiated directly.
		Compiled = 0x00000002,	// Script has been compiled successfully.
		Config = 0x00000004,	// Load object configuration at construction time.
		Transient = 0x00000008,	// This object type can't be saved; null it out at save time.
		Parsed = 0x00000010,	// Successfully parsed.
		Localized = 0x00000020,	// Class contains localized text.
		SafeReplace = 0x00000040,	// Objects of this class can be safely replaced with default or NULL.
		RuntimeStatic = 0x00000080,	// Objects of this class are static during gameplay.
		NoExport = 0x00000100,	// Don't export to C++ header.
		Placeable = 0x00000200,	// Allow users to create in the editor.
		PerObjectConfig = 0x00000400,	// Handle object configuration on a per-object basis, rather than per-class.
		NativeReplication = 0x00000800,	// Replication handled in C++.
		EditInlineNew = 0x00001000,	// Class can be constructed from editinline New button.
		CollapseCategories = 0x00002000,	// Display properties in the editor without using categories.
		IsAUProperty = 0x00008000,	// IsA UProperty
		IsAUObjectProperty = 0x00010000,	// IsA UObjectProperty
		IsAUBoolProperty = 0x00020000,	// IsA UBoolProperty
		IsAUState = 0x00040000,	// IsA UState
		IsAUFunction = 0x00080000,	// IsA UFunction

		NeedsDefProps = 0x00100000,	// Class needs its defaultproperties imported
		HasComponents = 0x00400000,	// Class has component properties.

		Hidden = 0x00800000,	// Don't show this class in the editor class browser or edit inline new menus.
		Deprecated = 0x01000000,	// Don't save objects of this class when serializing
		HideDropDown = 0x02000000,	// Class not shown in editor drop down for class selection

		Exported = 0x04000000,	// Class has been exported to a header file


		// Flags to inherit from base class.
		Inherit = Transient | Config | Localized | SafeReplace | RuntimeStatic | PerObjectConfig | Placeable | IsAUProperty | IsAUObjectProperty | IsAUBoolProperty | IsAUState | IsAUFunction | HasComponents | Deprecated,
		RecompilerClear = Inherit | Abstract | NoExport | NativeReplication,
		ScriptInherit = Inherit | EditInlineNew | CollapseCategories,
	};
}