#pragma once

#include <TypeAggregator/TArray.h>
#include <cstdlib>
#include <stdlib.h>

namespace TypeAggregator::Types::Core
{
	#define FSTRING_BASE_TYPE wchar_t

	struct FString : public TArray<wchar_t>
	{
		FString() {};

		FString(wchar_t* string)
		{
			ArrayMax = ArrayCount = sizeof(string);

			if (ArrayCount)
				this->Data = string;
		}

		FString(const wchar_t* string)
			: FString(const_cast<wchar_t*>(string)) {}

		FString(char* string)
		{
			size_t newsize = strlen(string) + 1;
			wchar_t* wcstring = new wchar_t[newsize];
#ifdef _WIN32
			size_t num = 0;
			mbstowcs_s(&num, wcstring, newsize, string, newsize - 1);
#else
			mbstowcs(wcstring, string, newsize);
#endif

			ArrayMax = ArrayCount = newsize;
			this->Data = wcstring;
		}

		FString(const char* string)
			: FString(const_cast<char*>(string)) {}

		FString(std::string string)
			: FString(string.c_str()) {}

		FString(std::wstring string)
			: FString(string.c_str()) {}

		const char* ToChar()
		{
			if (this->Data == NULL)
				return "";

			size_t size = (wcslen(this->Data) + 1) * sizeof(wchar_t);
			char* buffer = new char[size];
			wcstombs(buffer, this->Data, size);

#ifdef _WIN32
			//size_t convertedSize;
			//wcstombs_s(&convertedSize, buffer, size, this->Data, size);
#else
			wcstombs(buffer, this->Data, size);
#endif // _WIN32

			return buffer;
		}

		const char* ToChar() const
		{
			/*if (this->Data == NULL)
				return "";

			size_t size = (wcslen(this->Data) + 1) * sizeof(wchar_t);
			char* buffer = new char[size];

			//size_t convertedSize;
			//wcstombs_s(&convertedSize, buffer, size, this->Data, size);
			wcstombs(buffer, this->Data, size);

			return buffer;*/
			return this->ToChar();
		}

		operator const char* () const { return ToChar(); }
		operator char* () { return const_cast<char*>(ToChar()); }

		~FString() {};

		FString operator = (wchar_t* comparator)
		{
			if (this->Data != comparator)
			{
				ArrayMax = ArrayCount = sizeof(comparator);

				if (this->ArrayCount)
					this->Data = comparator;
			}

			return *this;
		}
	};
}