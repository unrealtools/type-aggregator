#pragma once

namespace TypeAggregator::Types::Core
{
	enum class EFunctionFlags : uint32_t
	{
		None = 0,
		Final = 0x00000001,	// Function is final (prebindable, non-overridable function).
		Defined = 0x00000002,	// Function has been defined (not just declared).
		Iterator = 0x00000004,	// Function is an iterator.
		Latent = 0x00000008,	// Function is a latent state function.
		PreOperator = 0x00000010,	// Unary operator is a prefix operator.
		Singular = 0x00000020,   // Function cannot be reentered.
		Net = 0x00000040,   // Function is network-replicated.
		NetReliable = 0x00000080,   // Function should be sent reliably on the network.
		Simulated = 0x00000100,	// Function executed on the client side.
		Exec = 0x00000200,	// Executable from command line.
		Native = 0x00000400,	// Native function.
		Event = 0x00000800,   // Event function.
		Operator = 0x00001000,   // Operator function.
		Static = 0x00002000,   // Static function.
		NoExport = 0x00004000,   // Don't export intrinsic function to C++.
		Const = 0x00008000,   // Function doesn't modify this object.
		Invariant = 0x00010000,   // Return value is purely dependent on parameters; no state dependencies or internal state changes.
		Public = 0x00020000,	// Function is accessible in all classes (if overridden, parameters much remain unchanged).
		Private = 0x00040000,	// Function is accessible only in the class it is defined in (cannot be overriden, but function name may be reused in subclasses.  IOW: if overridden, parameters don't need to match, and Super.Func() cannot be accessed since it's private.)
		Protected = 0x00080000,	// Function is accessible only in the class it is defined in and subclasses (if overridden, parameters much remain unchanged).
		Delegate = 0x00100000,	// Function is actually a delegate.
		NetServer = 0x00200000,	// Function is executed on servers (set by replication code if passes check)

		// Combinations of flags.
		FuncInherit = Exec | Event,
		FuncOverrideMatch = Exec | Final | Latent | PreOperator | Iterator | Static | Public | Protected,
		NetFuncFlags = Net | NetReliable,
	};
}
