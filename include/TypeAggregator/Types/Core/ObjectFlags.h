#pragma once

#include <cstdint>
#include <string>
#include <stdexcept>
#include <magic_enum.hpp>
#include <magic_enum_containers.hpp>

namespace TypeAggregator::Types::Core
{
	enum class EObjectFlags : uint32_t
	{
		NoFlags = 0,
		Public =  1 << 0,
		Standalone = 1 << 2,
		MarkAsNative = 1 << 3,
		Transactional =  1 << 4,
		ClassDefaultObject =  1 << 5,
		ArchetypeObject =  1 << 6,
		Transient = 1 << 7,
		MarkAsRootSet = 1 << 8,
		TagGarbageTemp = 1 << 9,
		NeedInitialization = 1 << 10,
		NeedLoad = 1 << 11,
		KeepForCooker = 1 << 12,
		NeedPostLoad = 1 << 13,
		NeedPostLoadSubobjects = 1 << 14,
		NewerVersionExists = 1 << 15,
		BeginDestroyed =  1 << 16,
		FinishDestroyed =  1 << 17,
		BeingRegenerated =  1 << 18,
		DefaultSubObject =  1 << 19,
		WasLoaded =  1 << 20,
		TextExportTransient =  1 << 21,
		LoadCompleted =  1 << 22,
		InheritableComponentTemplate = 1 << 23,
		DuplicateTransient = 1 << 24,
		StrongRefOnFrame = 1 << 25,
		NonPIEDuplicateTransient = 1 << 26,
		Dynamic = 1 << 27,
		WillBeLoaded =  1 << 28,
	};

}

template<>
struct magic_enum::customize::enum_range<TypeAggregator::Types::Core::EObjectFlags> {
	static constexpr bool is_flags = true;
};

