#pragma once

#include <cstdint>

namespace TypeAggregator::Types::Core
{
	enum class EPropertyFlags : uint64_t
	{
		None = 0,
		Edit = 0x1,
		Const = 0x2,
		Input = 0x4,
		ExportObject = 0x8,
		OptionalParm = 0x10,
		Net = 0x20,
		EditConstArray = 0x40,
		Parm = 0x80,
		OutParm = 0x100,
		SkipParm = 0x200,
		ReturnParm = 0x400,
		CoerceParm = 0x800,
		Native = 0x1000,
		Transient = 0x2000,
		Config = 0x4000,
		Localized = 0x8000,
		Travel = 0x10000,
		EditConst = 0x20000,
		GlobalConfig = 0x40000,
		Component = 0x80000,
		NeedCtorLink = 0x400000,
		NoExport = 0x800000,
		NoClear = 0x2000000,
		EditInline = 0x4000000,
		EdFindable = 0x8000000,
		EditInlineUse = 0x10000000,
		Deprecated = 0x20000000,
		EditInlineNotify = 0x40000000,
		RepNotify = 0x100000000,
		Interp = 0x200000000,
		NonTransactional = 0x400000000,
	};
}