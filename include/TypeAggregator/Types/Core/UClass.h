#pragma once

#include <TypeAggregator/Types/Core/UStruct.h>
#include <TypeAggregator/Types/Core/ClassFlags.h>

namespace TypeAggregator::Types::Core
{
    class UClass : public UStruct
    {
    public:
        UClass(uintptr_t address)
            : UStruct(address) {}

        uint32_t GetClassFlags();

        UClass* GetDefaultClass();

        bool HasClassFlags(EClassFlags flags);
        std::vector<std::string> GetClassFlagsList();

        STATIC_CLASS("Core.Class");
    };
}