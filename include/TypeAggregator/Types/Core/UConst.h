#pragma once

#include <TypeAggregator/Types/Core/UField.h>
#include <TypeAggregator/Types/Core/FString.h>

namespace TypeAggregator::Types::Core
{
	class UConst : public UField
	{
	public:
		FString GetValue();

		STATIC_CLASS("Core.Const");
	};
}