#pragma once

#include <TypeAggregator/Types/Core/UField.h>
#include <TypeAggregator/Types/Core/UnrealNames.h>

namespace TypeAggregator::Types::Core
{
	class UEnum : public UField
	{
	public:
		TUnrealTypeArray<FName> GetNames();

		STATIC_CLASS("Core.Enum");
	};
}