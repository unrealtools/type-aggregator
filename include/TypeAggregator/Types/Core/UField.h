#pragma once

#include <TypeAggregator/Types/Core/UObject.h>

namespace TypeAggregator::Types::Core
{
	class UField : public UObject
	{
	public:
		UField(uintptr_t address) : UObject(address) {}

		UField* GetNext();

		STATIC_CLASS("Core.Field");
	};
}
