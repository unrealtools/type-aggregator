#pragma once

#include <TypeAggregator/Types/Core/UStruct.h>
#include <TypeAggregator/Types/Core/FunctionFlags.h>

namespace TypeAggregator::Types::Core
{
	class UFunction : public UStruct
	{
	public:
		uint32_t GetFunctionFlags();
		uint16_t GetNativeIndex();
		uintptr_t GetPointer();

		bool HasFunctionFlag(EFunctionFlags flag);
		std::vector<std::string> GetFunctionFlagsList();

		STATIC_CLASS("Core.Function");
	};
}