#pragma once
#include <cstdint>
#include <nlohmann/json.hpp>
#include <TypeAggregator/TypeDef/StructDef.h>
#include <TypeAggregator/Types/UnrealType.h>
#include <TypeAggregator/TUnrealTypeArray.h>
#include <TypeAggregator/Types/Core/ObjectFlags.h>

#define STATIC_CLASS(identifier) \
    inline static const std::string ClassName = identifier; \
    inline static UClass* StaticClass() { \
        static UClass* pStaticClass = nullptr; \
        if(!pStaticClass) \
            pStaticClass = (UClass*)FindObject(identifier); \
        return pStaticClass; \
    }

namespace TypeAggregator::Types::Core
{
    using json = nlohmann::json;

    class UClass;

    class UObject : public UnrealType
    {
    public:
        using UnrealType::UnrealType;

    public:
        std::string GetName();
        std::string GetPackage();

        uint32_t GetObjectInternalInteger();
        uint32_t GetObjectFlags();
        UObject* GetOuter();
        UClass* GetClass();
        UObject* GetObjectArchetype();

        bool HasObjectFlags(EObjectFlags flags);
        std::vector<std::string> GetObjectFlagsList();

        std::string GetFullName();
        std::string GetNameCPP();
        std::string GetFullNameCPP();

        static UClass* FindClass(std::string className);
        static UObject* FindObject(std::string objectName);

        bool IsA(UClass* cls);
        bool IsA(std::string className);

        template<typename T>
        requires(std::is_base_of_v<UObject, T>)
        bool IsA() 
        { 
            return IsA(T::StaticClass());
        }

        STATIC_CLASS("Core.Object");
    };
}