#pragma once

#include <TypeAggregator/Types/Core/UField.h>
#include <TypeAggregator/Types/Core/PropertyFlags.h>

namespace TypeAggregator::Types::Core
{
	class UProperty : public UField
	{
	public:
		uint32_t GetOffset();
		uint32_t GetPropertyFlags();
		uint32_t GetPropertySize();
		std::string GetType();

		std::string GetDefaultValue();

		bool HasPropertyFlag(EPropertyFlags flag);
		std::vector<std::string> GetPropertyFlagsList();

		STATIC_CLASS("Core.Property");
	};
}