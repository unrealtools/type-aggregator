#pragma once

#include <TypeAggregator/Types/Core/UStruct.h>

namespace TypeAggregator::Types::Core
{
	class UScriptStruct : public UStruct
	{
	public:

		STATIC_CLASS("Core.ScriptStruct");
	};
}