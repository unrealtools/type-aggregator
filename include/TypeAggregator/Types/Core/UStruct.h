#pragma once

#include <TypeAggregator/Types/Core/UField.h>
#include <TypeAggregator/Types/Core/UProperty.h>

namespace TypeAggregator::Types::Core
{
	class UField;

	class UStruct : public UField
	{
	public:
		UStruct(uintptr_t address) 
			: UField(address) {}

		UField* GetSuperField();
		UField* GetChildren();

		uint32_t GetPropertySize();

		std::vector<UProperty*> GetProperties();

		STATIC_CLASS("Core.Struct");
	};
}