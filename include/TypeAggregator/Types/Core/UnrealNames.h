#pragma once

#include <TypeAggregator/TypeDef/StructDef.h>
#include <TypeAggregator/Types/UnrealType.h>
#include <TypeAggregator/TUnrealTypeArray.h>

namespace TypeAggregator::Types::Core
{
    struct FNameEntry : public UnrealType
    {
        using UnrealType::UnrealType;

        uint32_t GetIndex()
        {
            return Read<uint32_t, FNameEntry>("Index");
        }

        std::string GetName()
        {
            char* n = Read<char*, FNameEntry>("Name");
            return std::string(n);
        }

        inline static const std::string ClassName = "Core.NameEntry";
    };

    struct FName : public UnrealType
    {
        using UnrealType::UnrealType;

        uint32_t GetIndex()
        {
            return Read<uint32_t, FName>("Index");
        }

        std::string GetName();

        inline static const std::string ClassName = "Core.Name";
    };
}