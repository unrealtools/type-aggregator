#pragma once

#include <TypeAggregator/TUnrealTypeArray.h>
#include <TypeAggregator/Types/Core/UnrealNames.h>
#include <TypeAggregator/Types/Core/UObject.h>
#include <map>

namespace TypeAggregator::Types
{
	class GlobalStore
	{
	public:
		static TypeAggregator::TUnrealTypeArray<Core::FNameEntry*, uint32_t>* Names();
		static TypeAggregator::TUnrealTypeArray<Core::UObject*, uint32_t>* Objects();

		inline static void SetNames(uintptr_t ptr)
		{
			GNames = new TypeAggregator::TUnrealTypeArray<Core::FNameEntry*, uint32_t>(ptr);
		}

		inline static void SetObjects(uintptr_t ptr)
		{
			GObjects = new TypeAggregator::TUnrealTypeArray<Core::UObject*, uint32_t>(ptr);
		}

		inline static std::unordered_map<std::string, Core::UObject*> ObjectCache = {};

	protected:
		inline static TypeAggregator::TUnrealTypeArray<Core::FNameEntry*, uint32_t>* GNames = nullptr;
		inline static TypeAggregator::TUnrealTypeArray<Core::UObject*, uint32_t>* GObjects = nullptr;

	};
}
