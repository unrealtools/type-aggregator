#pragma once

#include <cstdint>
#include <string>
#include <stdexcept>
#include <typeinfo>

#include <TypeAggregator/TypeDef/PropertyDef.h>
#include <TypeAggregator/TypeDef/StructDef.h>
#include <TypeAggregator/TypeDef/Registry.h>

namespace TypeAggregator::Types
{
    using namespace TypeAggregator::TypeDef;

    class UnrealType
    {
    protected:
        uintptr_t Address;
        uintptr_t RegionEnd;

    public:
        UnrealType() : Address(0), RegionEnd(0) {}

        UnrealType(uintptr_t Address)
            : Address(Address)
        {
            RegionEnd = CheckAddressSpace(Address);
        }

        uintptr_t GetAddress() { return Address; }
        static uintptr_t CheckAddressSpace(uintptr_t address);

    protected:
        template<typename TPropertyType>
        TPropertyType Read(uintptr_t Offset)
        {
            uintptr_t fullAddress = Address + Offset;

            // prevent dereferencing non-existant memory
            if ((sizeof(TPropertyType) + fullAddress) > RegionEnd)
                throw std::out_of_range("Address is out of memory!");

            return *(TPropertyType*)(Address + Offset);
        }

        template<>
        char* Read(uintptr_t Offset)
        {
            return (char*)(Address + Offset);
        }

        template<typename TPropertyType, typename TStructType>
        TPropertyType Read(std::string PropertyName)
        {
            auto propDef = GetPropertyDefByName<TStructType>(PropertyName);
            return Read<TPropertyType>(propDef->Offset);
        }

        template<typename TPropertyType>
        TPropertyType Read(std::string StructName, std::string PropertyName)
        {
            auto propertyDef = GetPropertyDefByName(StructName, PropertyName);
            return Read<TPropertyType>(propertyDef->Offset);
        }

        template<typename TPropertyType>
        requires(std::is_base_of_v<UnrealType, TPropertyType>)
        TPropertyType Read(uintptr_t Offset)
        {
            return TPropertyType(Address + Offset);
        }

        template<typename TPropertyType>
        requires(std::is_pointer_v<TPropertyType> && std::is_base_of_v<UnrealType, std::remove_pointer_t<TPropertyType>>)
        TPropertyType Read(uintptr_t Offset)
        {
            return new std::remove_pointer_t<TPropertyType>(Read<uintptr_t>(Offset));
        }

        template<typename TStructType>
        PropertyDef* GetPropertyDefByName(std::string PropertyName)
        {
            auto typeDef = TypeRegistry::GetTypeDef<TStructType>();
            PropertyDef* propDef = typeDef->GetPropertyByName(PropertyName);

            if (propDef == nullptr)
            {
                const std::type_info& structTypeInfo = typeid(TStructType);
                throw std::invalid_argument("PropertyDef " + PropertyName + " not found in " + structTypeInfo.name());
            }

            return propDef;
        }

        PropertyDef* GetPropertyDefByName(std::string StructName, std::string PropertyName)
        {
            auto typeDef = TypeRegistry::GetTypeDef(StructName);
            auto propertyDef = typeDef->GetPropertyByName(PropertyName);

            return propertyDef;
        }
    };
}