#pragma once
#include <cstdint>
#include <TypeAggregator/Types/GlobalStore.h>
#define NOMINMAX
#include <Windows.h>
#include <TypeAggregator/Memory.h>
#include <spdlog/spdlog.h>
using namespace TypeAggregator::Types;

namespace TypeAggregator
{
	class offset_exception : public std::exception
	{
	private:
		std::string offset_name;
		std::string additional_info;
		std::string message;

	public:
		offset_exception(std::string offsetName, std::string additionalInfo = "")
			: offset_name(offsetName), additional_info(additionalInfo) 
		{
			message = fmt::format("Failed to find offset of {}. {}", offset_name, additional_info);
		}

		virtual const char* what()
		{
			return message.c_str();
		}
	};

	class UnrealScanner
	{
	protected:
		size_t namePropOffset = 0;
		size_t indexPropOffset = 0;
		size_t structPropSizeOffset = 0;
		size_t objectOuterOffset = 0;

		inline static char nonePattern[] = "\x4E\x6f\x6e\x65";
		inline static char intPropPattern[] = "\x49\x6e\x74\x50\x72\x6f\x70\x65\x72\x74\x79";
		inline static char bytePropPattern[] = "\x42\x79\x74\x65\x50\x72\x6f\x70\x65\x72\x74\x79";

		inline static char globalObjectsPattern[] = "\xA1\x00\x00\x00\x00\x8B\x00\x00\x8B\x00\x00\x25";
		inline static const char* globalObjectsMask = "x????x??x??x";
		inline static char globalNamesPattern[] = "\x8B\x0D\x00\x00\x00\x00\x83\x3C\x81\x00\x74";
		inline static const char* globalNamesMask = "xx????xxxxx";

	public:
		TArray<uintptr_t>* GlobalObjects = 0;
		TArray<uintptr_t>* GlobalNames = 0;
		size_t FNameEntryNameOffset = 0;

	public:
		int FindTArrayIndexOffset(uintptr_t* arrayData, bool isObjectArray = false, size_t startOffset = 0, size_t maxOffset = 0x60, size_t maxIndex = 0x20);

		int FindObjectInternalIntegerOffset(uintptr_t* GObjects, size_t startOffset = 0, size_t maxOffset = 0x40, size_t maxObjects = 0x20);
		int FindObjectNameOffset();
		int FindObjectOuterOffset();
		int FindObjectClassOffset();

		int FindFieldNextOffset();

		int FindStructSuperFieldOffset();
		int FindStructPropSizeOffset();
		int FindStructChildOffset();

		int FindPropertySizeOffset(size_t childOffset, size_t nextOffset);
		int FindPropertyOffsetOffset();
		int FindPropertyFlagsOffset();

		int FindObjectPropertyClassOffset();
		int FindClassPropertyClassOffset();
		int FindStructPropertyStructOffset();
		int FindArrayPropertyInnerPropOffset();

		int FindClassFlagsOffset();
		int FindClassDefaultObjectOffset();

		int FindEnumNamesOffset();
		int FindConstValueOffset();

		int FindFunctionNativeIndexOffset();
		int FindFunctionAddressOffset();
		int FindFunctionFlagsOffset();

		int FindGlobalNames();
		int FindGlobalNamesByPattern();

		int FindGlobalObjectsByPattern();
		int FindGlobalObjects(int GNames);
		int ValidateGlobalObjectsArray(uintptr_t gObjectsPtr);

		bool IsValidObjectPointer(uintptr_t objPtr);
		bool IsValidPointer(uintptr_t pointer);
		bool IsValidAddress(uintptr_t address);

		int FindObjectInternalInteger(std::string name, int searchOffset = 0);
		int FindObjectInternalInteger(int nameIndex, int searchOffset = 0);

		int FindNameIndex(std::string search, int searchOffset = 0);
		int FindFullNameIndex(std::string search, int searchOffset = 0);

		template<typename TVal>
		size_t MemSearch(TVal val, uintptr_t start, size_t len, size_t offset = 0);

		template<typename TVal, std::size_t valCount>
		int FindCommonOffset(const uintptr_t(&locations)[valCount], const TVal(&values)[valCount], std::size_t scanRange);

		template<typename TVal, std::size_t valCount>
		int FindCommonObjectOffset(const std::string(&objectNames)[valCount], const TVal(&values)[valCount], std::size_t scanRange = 0x150);

		int GetStructSize(std::string structName);

		std::string GetObjectFullName(uintptr_t objectAddr);
		std::string GetObjectName(uintptr_t objectAddr);
	};

	template<typename TVal, std::size_t valCount>
	int UnrealScanner::FindCommonOffset(const uintptr_t(&locations)[valCount], const TVal(&values)[valCount], std::size_t scanRange)
	{
		int offset = 0;
		do
		{
			// search for next offset in all locations
			std::vector<int> offsets = {};
			for (int i = 0; i < valCount; i++)
				offsets.push_back(MemSearch<TVal>(values[i], locations[i], scanRange, offset + 1));

			// if all offsets are equal offset is found
			if (std::equal(offsets.begin() + 1, offsets.end(), offsets.begin()))
				return offsets[0];

			// if offsets are not equal, skip to the next highest offset found
			offset = (*std::max_element(offsets.begin() + 1, offsets.end())) - 1;
		} while (offset > 0);

		return -1;
	}

	template<typename TVal, std::size_t valCount>
	int UnrealScanner::FindCommonObjectOffset(const std::string(&objectNames)[valCount], const TVal(&values)[valCount], std::size_t scanRange)
	{
		uintptr_t locations[valCount];

		for (int i = 0; i < valCount; i++)
		{
			int objIndex = FindFullNameIndex(objectNames[i]);
			if (objIndex == -1)
			{
				spdlog::error("Failed to find object {}", objectNames[i]);
				return -1;
			}

			locations[i] = GlobalObjects->at(objIndex);
		}

		return FindCommonOffset(locations, values, scanRange);
	}


	template<typename TVal>
	size_t UnrealScanner::MemSearch(TVal val, uintptr_t start, size_t len, size_t offset)
	{
		for (int i = (start + offset); i < (start + len - sizeof(TVal)); i++)
			if (*(TVal*)(i) == val)
				return i - start;

		return 0;
	}
}