#include <TypeAggregator/Memory.h>
#include <Psapi.h>

#pragma warning(disable: 4018)
#pragma comment( lib, "PSAPI.lib" )

using namespace std;

namespace TypeAggregator {
    Memory* Memory::m_Memory = NULL;

    Memory* Memory::instance() {
        if (m_Memory == NULL) {
            m_Memory = new Memory();
        }
        return m_Memory;
    }

    bool Memory::isPageReadable(MEMORY_BASIC_INFORMATION* mbi) {
        bool bRet = false;

        if (mbi->Protect & PAGE_GUARD)
            return false;

        if ((mbi->Protect & PAGE_EXECUTE_READ) ||
            (mbi->Protect & PAGE_EXECUTE_READWRITE) ||
            (mbi->Protect & PAGE_EXECUTE_WRITECOPY) ||
            (mbi->Protect & PAGE_READONLY) ||
            (mbi->Protect & PAGE_READWRITE) ||
            (mbi->Protect & PAGE_WRITECOPY))
            bRet = true;

        /* false
        #define PAGE_NOACCESS          0x01
        #define PAGE_EXECUTE           0x10
        #define PAGE_GUARD            0x100
        #define PAGE_NOCACHE          0x200
        #define PAGE_WRITECOMBINE     0x400
        */
        return bRet;
    }

    bool Memory::getModuleInfo(char* pModuleName, ModuleInfo* moduleInfo) {
        uintptr_t offset;
        offset = (uintptr_t)GetModuleHandle(pModuleName);
        if (offset) {
            return getModuleInfo((void*)offset, moduleInfo);
        }
        return false;
    }

    bool Memory::getModuleInfo(void* pOffset, ModuleInfo* moduleInfo) {
        MEMORY_BASIC_INFORMATION MBI;
        uintptr_t curAddy = 0;
        moduleInfo->modBase = 0;
        moduleInfo->modSize = 0;
        moduleInfo->protectedAreas.clear();

        //Get module base 
        size_t ret = VirtualQuery(pOffset, &MBI, sizeof(MEMORY_BASIC_INFORMATION));
        if (!ret /*no bytes in mbi*/ || !MBI.AllocationBase /*no module*/ || MBI.State == MEM_FREE /*free memory*/ || MBI.State == MEM_RESERVE /*not used memory*/ || MBI.RegionSize == 0 /*bad size*/) {
            return false;
        }

        //we have a module and non-free and non-reserved memory
        //set the modulebase, do not save the size as it will be counted based on the different regions
        moduleInfo->modBase = (uintptr_t)MBI.AllocationBase;
        curAddy = (uintptr_t)MBI.AllocationBase;

        for (;; curAddy += MBI.RegionSize) {
            VirtualQuery((void*)curAddy, &MBI, sizeof(MEMORY_BASIC_INFORMATION));

            //are we still in the same module?
            if ((uintptr_t)MBI.AllocationBase != moduleInfo->modBase) {
                return moduleInfo->modSize > 0;
            }

            //do we have proper memory?
            if (!MBI.RegionSize || MBI.State == MEM_FREE || MBI.State == MEM_RESERVE) {
                return moduleInfo->modSize > 0;
            }

            //we have a region of the current module
            //which proper memory

            //add the size to the modules full size
            moduleInfo->modSize += MBI.RegionSize;

            //check the protection of the region
            if (!isPageReadable(&MBI)) {
                ProtectedArea protectedArea;

                //base address holds the start of this region
                protectedArea.start = (uintptr_t)MBI.BaseAddress;
                protectedArea.size = (size_t)MBI.RegionSize;
                protectedArea.end = (uintptr_t)MBI.BaseAddress + MBI.RegionSize - 1;
                moduleInfo->protectedAreas.push_back(protectedArea);
            }
        }

        return moduleInfo->modSize > 0;
    }

    bool Memory::getModuleInfoOfAllModules(vector<Memory::ModuleInfo>* moduleInfos) {
        uintptr_t curAddy = 0x0;
        Memory::ModuleInfo curModule;

        SYSTEM_INFO SI;
        GetSystemInfo(&SI);

        while (curAddy < 0x7FFF0000) {
            if (getModuleInfo((void*)curAddy, &curModule)) {
                moduleInfos->push_back(curModule);
                curAddy += curModule.modSize + 1;
            }
            else {
                curAddy += SI.dwPageSize;
            }
        }

        return moduleInfos->size() > 0;
    }

    uintptr_t Memory::findPattern(ModuleInfo* moduleInfo, BYTE* pattern, char* mask, size_t startOffset) {
        return findPattern(moduleInfo, (char*)pattern, mask, startOffset);
    }

    uintptr_t Memory::findPattern(ModuleInfo* moduleInfo, char* pattern, char* mask, size_t startOffset) {
        //Get pageSize
        SYSTEM_INFO systemInfo;
        GetSystemInfo(&systemInfo);
        size_t pageSize = (size_t)systemInfo.dwPageSize;

        size_t byteMatchedIndex = 0;
        size_t searchLen = strlen(mask) - 1;

        if (startOffset != NULL && (startOffset < moduleInfo->modBase || startOffset > moduleInfo->modBase + moduleInfo->modSize)) {
            return NULL;
        }

        uintptr_t start = startOffset != 0 ? startOffset : moduleInfo->modBase;
        MEMORY_BASIC_INFORMATION mbi;
        if (!VirtualQuery((LPVOID)start, &mbi, sizeof(mbi)))
            return 0;

        if (mbi.State != MEM_COMMIT || !isPageReadable(&mbi))
            return 0;

        uintptr_t currRegionEnd = (uintptr_t)mbi.BaseAddress + mbi.RegionSize;

        for (uintptr_t scanAddress = startOffset != NULL ? startOffset : moduleInfo->modBase; scanAddress < (moduleInfo->modBase + moduleInfo->modSize - searchLen); scanAddress++) 
        {
            if (scanAddress + searchLen >= currRegionEnd)
            {
                do
                {
                    scanAddress = (uintptr_t)mbi.BaseAddress + mbi.RegionSize;
                    if (!VirtualQuery((LPVOID)scanAddress, &mbi, sizeof(mbi)))
                        return 0;
                } while (mbi.State != MEM_COMMIT || !isPageReadable(&mbi));

                currRegionEnd = (uintptr_t)mbi.BaseAddress + mbi.RegionSize;
            }

            //Do scan
            if (*(BYTE*)scanAddress == (BYTE)pattern[byteMatchedIndex] || mask[byteMatchedIndex] == '?') {
                if (mask[byteMatchedIndex + 1] == '\0') {
                    return (scanAddress - searchLen);
                }
                byteMatchedIndex++;
            }
            else {
                byteMatchedIndex = 0;
            }
        }
        return NULL;
    }

    uintptr_t Memory::findPattern(std::vector<ModuleInfo>* moduleInfos, char* pattern, char* mask, size_t startOffset)
    {
        uintptr_t ret = 0;

        for (auto& moduleInfo : *moduleInfos)
        {
            if (startOffset > (moduleInfo.modBase + moduleInfo.modSize))
                continue;

            ret = this->findPattern(&moduleInfo, pattern, mask, startOffset > moduleInfo.modBase ? startOffset : moduleInfo.modBase);
            if (ret != 0)
                return ret;
        }

        return 0;
    }
}