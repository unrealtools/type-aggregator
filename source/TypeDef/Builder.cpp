#include <TypeAggregator/TypeDef/Builder.h>
#include <TypeAggregator/TypeDef/Registry.h>
#include <TypeAggregator/Types/Core/UClass.h>
#include <TypeAggregator/Types/Core/UnrealNames.h>
#include <TypeAggregator/Types/Core/UFunction.h>

using namespace TypeAggregator;
using namespace TypeAggregator::TypeDef;
using namespace TypeAggregator::Types::Core;

TypeBuilder::TypeBuilder(UnrealScanner* scanner)
{
	this->Scanner = scanner;
}

void TypeBuilder::BuildNameDef()
{
	auto nameDef = new StructDef("Name", "Core", "", Scanner->FindObjectInternalInteger("Name"));
	nameDef->Size = 0x8;
	nameDef->Properties = {
		PropertyDef("Index", 0x0, "int", sizeof(int))
	};
	TypeRegistry::AddTypeDef<FName>(nameDef);
}

void TypeBuilder::BuildNameEntryDef()
{
	auto nameEntryDef = new StructDef("NameEntry", "Core", "", Scanner->FindObjectInternalInteger("NameEntry"));
	nameEntryDef->Size = Scanner->FNameEntryNameOffset + sizeof(char*);
	nameEntryDef->Properties = {
		PropertyDef("Name", Scanner->FNameEntryNameOffset, "char*", sizeof(char*))
	};
	TypeRegistry::AddTypeDef<FNameEntry>(nameEntryDef);
}

void TypeBuilder::BuildStringDef()
{
	auto stringDef = new StructDef("String", "Core", "", -1);
	stringDef->Size = TypeRegistry::GetTypeDef("Core.Array")->Size;
	stringDef->Properties = {
		PropertyDef("String", 0, "Core.TArray<wchar_t>", stringDef->Size)
	};

	TypeRegistry::AddTypeDef("Core.String", stringDef);
}

void TypeBuilder::BuildArrayDef()
{
	auto arrayDef = new StructDef("Array", "Core", "", -1);
	arrayDef->Size = sizeof(uintptr_t) + (sizeof(int) * 2);
	arrayDef->Properties = {
		PropertyDef("Data", 0, "void*", sizeof(uintptr_t)),
		PropertyDef("Count", sizeof(uintptr_t), "int", sizeof(int)),
		PropertyDef("Max", sizeof(uintptr_t) + sizeof(int), "int", sizeof(int))
	};
	TypeRegistry::AddTypeDef("Core.Array", arrayDef);
}

void TypeBuilder::BuildScriptDelegateDef()
{
	auto scriptDelDef = new StructDef("ScriptDelegate", "Core", "", -1);
	scriptDelDef->Size = sizeof(uintptr_t) + TypeRegistry::GetTypeDef<FName>()->Size;
	scriptDelDef->Properties = {
		PropertyDef("Object", 0, "Core.UObject*", sizeof(uintptr_t)),
		PropertyDef("FunctionName", sizeof(uintptr_t), "Core.FName", TypeRegistry::GetTypeDef<FName>()->Size)
	};
	TypeRegistry::AddTypeDef("Core.ScriptDelegate", scriptDelDef);
}

void TypeBuilder::BuildFrameDef()
{
	auto frameDef = new StructDef("Frame", "Core", "", -1);
	frameDef->Size = 0x24;
	frameDef->Properties = {
		PropertyDef("Node", 16, "Core.UStruct*", sizeof(uintptr_t)),
		PropertyDef("Object", 16 + sizeof(uintptr_t), "Core.UObject*", sizeof(uintptr_t)),
		PropertyDef("Code", 16 + (sizeof(uintptr_t) * 2), "char*", sizeof(uintptr_t)),
		PropertyDef("Locals", 16 + (sizeof(uintptr_t) * 3), "char*", sizeof(uintptr_t)),
		PropertyDef("PreviousFrame", 16 + (sizeof(uintptr_t) * 4), "Core.FFrame*", sizeof(uintptr_t))
	};
	TypeRegistry::AddTypeDef("Core.Frame", frameDef);
}

void TypeBuilder::BuildObjectDef()
{
	auto objectDef = new StructDef("Object", "Core", "", Scanner->FindObjectInternalInteger("Object"));
	objectDef->Properties = {
		PropertyDef("ObjectInternalInteger", Scanner->FindObjectInternalIntegerOffset(const_cast<uintptr_t*>(Scanner->GlobalObjects->GetData())), "int", sizeof(int)),
		PropertyDef("Name", Scanner->FindObjectNameOffset(), "Core.FName", TypeRegistry::GetTypeDef("Core.Name")->Size),
		PropertyDef("Class", Scanner->FindObjectClassOffset(), "Core.UClass*", sizeof(uintptr_t)),
		PropertyDef("Outer", Scanner->FindObjectOuterOffset(), "Core.UClass*", sizeof(uintptr_t))
	};
	TypeRegistry::AddTypeDef<UObject>(objectDef);
}

void TypeBuilder::BuildFieldDef()
{
	auto fieldDef = new StructDef("Field", "Core", "Core.UObject", Scanner->FindObjectInternalInteger("Field"));
	fieldDef->Properties = {
		PropertyDef("Next", Scanner->FindFieldNextOffset(), "Core.UField*", sizeof(uintptr_t))
	};
	TypeRegistry::AddTypeDef<UField>(fieldDef);
}

void TypeBuilder::BuildStructDef()
{
	auto structDef = new StructDef("Struct", "Core", "Core.UField", Scanner->FindObjectInternalInteger("Struct"));
	structDef->Properties = {
		PropertyDef("SuperField", Scanner->FindStructSuperFieldOffset(), "Core.UField*", sizeof(uintptr_t)),
		PropertyDef("Children", Scanner->FindStructChildOffset(), "Core.UField*", sizeof(uintptr_t)),
		PropertyDef("PropertySize", Scanner->FindStructPropSizeOffset(), "int", sizeof(int))
	};
	TypeRegistry::AddTypeDef<UStruct>(structDef);
}

void TypeBuilder::BuildClassDef()
{
	auto classDef = new StructDef("Class", "Core", "Core.UState", Scanner->FindObjectInternalInteger("Class"));
	classDef->Properties = {
		PropertyDef("ClassFlags", Scanner->FindClassFlagsOffset(), "int", sizeof(int)),
		PropertyDef("ClassDefaultObject", Scanner->FindClassDefaultObjectOffset(), "Core.UClass*", sizeof(uintptr_t))
	};
	TypeRegistry::AddTypeDef<UClass>(classDef);
}

void TypeBuilder::BuildPropertyDef()
{
	auto fieldDef = TypeRegistry::GetTypeDef<UField>();
	auto structDef = TypeRegistry::GetTypeDef<UStruct>();

	auto propDef = new StructDef("Property", "Core", "Core.UField", Scanner->FindObjectInternalInteger("Property"));
	propDef->Properties = {
		PropertyDef("PropertySize", Scanner->FindPropertySizeOffset(structDef->GetPropertyByName("Children")->Offset, fieldDef->GetPropertyByName("Next")->Offset), "int", sizeof(int)),
		PropertyDef("Offset", Scanner->FindPropertyOffsetOffset(), "int", sizeof(int)),
		PropertyDef("PropertyFlags", Scanner->FindPropertyFlagsOffset(), "int", sizeof(int))
	};
	TypeRegistry::AddTypeDef<UProperty>(propDef);
}

void TypeBuilder::BuildConstDef()
{
	auto constDef = new StructDef("Const", "Core", "Core.UField", Scanner->FindObjectInternalInteger("Const"));
	constDef->Properties = {
		PropertyDef("Value", Scanner->FindConstValueOffset(), "Core.FString", TypeRegistry::GetTypeDef("Core.String")->Size)
	};
	TypeRegistry::AddTypeDef("Core.Const", constDef);
}

void TypeBuilder::BuildEnumDef()
{
	auto enumDef = new StructDef("Enum", "Core", "Core.UField", Scanner->FindObjectInternalInteger("Enum"));
	enumDef->Properties = {
		PropertyDef("Names", Scanner->FindEnumNamesOffset(), "Core.TArray<Core.FName>", TypeRegistry::GetTypeDef("Core.Array")->Size)
	};
	TypeRegistry::AddTypeDef("Core.Enum", enumDef);
}

void TypeBuilder::BuildFunctionDef()
{
	auto funcDef = new StructDef("Function", "Core", "Core.UState", Scanner->FindObjectInternalInteger("Function"));
	funcDef->Properties = {
		PropertyDef("FunctionFlags", Scanner->FindFunctionFlagsOffset(), "int", sizeof(int)),
		PropertyDef("Address", Scanner->FindFunctionAddressOffset(), "void*", sizeof(uintptr_t))
	};
	TypeRegistry::AddTypeDef<UFunction>(funcDef);
}

void TypeBuilder::BuildObjectPropertyDef()
{
	auto objectPropDef = new StructDef("ObjectProperty", "Core", "Core.UProperty", Scanner->FindObjectInternalInteger("ObjectProperty"));
	objectPropDef->Properties = {
		PropertyDef("PropertyClass", Scanner->FindObjectPropertyClassOffset(), "Core.UClass*", sizeof(uintptr_t))
	};
	TypeRegistry::AddTypeDef("Core.ObjectProperty", objectPropDef);
}

void TypeBuilder::BuildClassPropertyDef()
{
	auto classPropDef = new StructDef("ClassProperty", "Core", "Core.UObjectProperty", Scanner->FindObjectInternalInteger("ClassProperty"));
	classPropDef->Properties = {
		PropertyDef("MetaClass", Scanner->FindClassPropertyClassOffset(), "Core.UClass*", sizeof(uintptr_t))
	};
	TypeRegistry::AddTypeDef("Core.ClassProperty", classPropDef);
}

void TypeBuilder::BuildInterfacePropertyDef()
{
	auto objectPropDef = TypeRegistry::GetTypeDef("Core.ObjectProperty");

	auto interfacePropDef = new StructDef("InterfaceProperty", "Core", "Core.UProperty", Scanner->FindObjectInternalInteger("InterfaceProperty"));
	interfacePropDef->Properties = {
		PropertyDef("InterfaceClass", objectPropDef->Properties[0].Offset, "Core.UClass*", sizeof(uintptr_t)) // TODO: find InterfaceClass by heuristics
	};
	TypeRegistry::AddTypeDef("Core.InterfaceProperty", interfacePropDef);
}

void TypeBuilder::BuildArrayPropertyDef()
{
	auto arrayPropDef = new StructDef("ArrayProperty", "Core", "Core.UProperty", Scanner->FindObjectInternalInteger("ArrayProperty"));
	arrayPropDef->Properties = {
		PropertyDef("InnerProperty", Scanner->FindArrayPropertyInnerPropOffset(), "Core.UProperty*", sizeof(uintptr_t))
	};
	TypeRegistry::AddTypeDef("Core.ArrayProperty", arrayPropDef);
}

void TypeBuilder::BuildMapPropertyDef()
{
	auto objectPropDef = TypeRegistry::GetTypeDef("Core.ObjectProperty");

	auto mapPropDef = new StructDef("MapProperty", "Core", "Core.UProperty", Scanner->FindObjectInternalInteger("MapProperty"));
	mapPropDef->Properties = {
		PropertyDef("Key", objectPropDef->Properties[0].Offset, "Core.UProperty*", sizeof(uintptr_t)),
		PropertyDef("Value", objectPropDef->Properties[0].Offset + sizeof(uintptr_t), "Core.UProperty*", sizeof(uintptr_t))
	};
	TypeRegistry::AddTypeDef("Core.MapProperty", mapPropDef);
}

void TypeBuilder::BuildStructPropertyDef()
{
	auto structPropDef = new StructDef("StructProperty", "Core", "Core.UProperty", Scanner->FindObjectInternalInteger("StructProperty"));
	structPropDef->Properties = {
		PropertyDef("PropertyStruct", Scanner->FindStructPropertyStructOffset(), "Core.UStruct*", sizeof(uintptr_t))
	};
	TypeRegistry::AddTypeDef("Core.StructProperty", structPropDef);
}

void TypeBuilder::BuildBytePropertyDef()
{
	auto bytePropDef = new StructDef("ByteProperty", "Core", "Core.UProperty", Scanner->FindObjectInternalInteger("ByteProperty"));
	auto objectPropDef = TypeRegistry::GetTypeDef("Core.ObjectProperty");

	bytePropDef->Properties = {
		PropertyDef("Enum", objectPropDef->Properties[0].Offset, "Core.Enum*", sizeof(uintptr_t))
	};
	TypeRegistry::AddTypeDef("Core.ByteProperty", bytePropDef);
}

void TypeBuilder::BuildAll()
{
	BuildNameDef();
	BuildNameEntryDef();
	BuildArrayDef();
	BuildStringDef();
	BuildScriptDelegateDef();
	BuildFrameDef();

	BuildObjectDef();
	BuildFieldDef();
	BuildStructDef();
	BuildClassDef();
	BuildPropertyDef();
	BuildConstDef();
	BuildEnumDef();
	BuildFunctionDef();

	BuildObjectPropertyDef();
	BuildClassPropertyDef();
	BuildInterfacePropertyDef();
	BuildArrayPropertyDef();
	BuildMapPropertyDef();
	BuildStructPropertyDef();
	BuildBytePropertyDef();

	auto objectDef = TypeRegistry::GetTypeDef<UObject>();
	auto object = (UClass*)GlobalStore::Objects()->at(Scanner->FindObjectInternalInteger("Object"));

	std::vector<PropertyDef> objPropDefs = {};
	for (auto& prop : object->GetProperties())
		objPropDefs.push_back(prop);

	objectDef->Properties = objPropDefs;
}