#include <TypeAggregator/TypeDef/ClassDef.h>
#include <TypeAggregator/Types/Core/UClass.h>
#include <TypeAggregator/Types/Core/UFunction.h>
#include <TypeAggregator/Types/Core/UConst.h>
#include <TypeAggregator/Types/Core/UEnum.h>
#include <TypeAggregator/Types/Core/UScriptStruct.h>
#include <TypeAggregator/EnumHelpers.h>

using namespace TypeAggregator::TypeDef;
using namespace TypeAggregator::Types::Core;

ClassDef::ClassDef(UClass* cls)
	//: StructDef((UStruct*)cls)
{
	auto superField = cls->GetSuperField();
	Name = cls->GetNameCPP();
	ParentName = superField->GetAddress() != 0 ? superField->GetFullNameCPP() : "";
	PackageName = cls->GetPackage();
	ObjectInternalInteger = cls->GetObjectInternalInteger();
	Size = cls->GetPropertySize();
	StructSize = superField->GetAddress() != 0 ? Size - UStruct(superField->GetAddress()).GetPropertySize() : Size;
	ObjectFlags = cls->GetObjectFlagsList();
	ClassFlags = cls->GetClassFlagsList();

	auto defaultClass = cls->GetDefaultClass();
	if (defaultClass && defaultClass->GetAddress() != 0)
	{
		DefaultClassIndex = defaultClass->GetObjectInternalInteger();
		VTableAddress = *(uintptr_t*)defaultClass->GetAddress();
	}
	else 
	{
		DefaultClassIndex = -1;
		VTableAddress = 0;
	}

	std::vector<PropertyDef> props;
	for (auto prop = (UProperty*)cls->GetChildren(); prop->GetAddress() != 0; prop = (UProperty*)prop->GetNext())
	{
		if (prop->IsA<UScriptStruct>())
			continue;
		if (prop->IsA<UFunction>())
			Functions.push_back((UFunction*)prop);
		else if (prop->IsA<UConst>())
			Constants[prop->GetName()] = ((UConst*)prop)->GetValue().ToChar();
		else if (prop->IsA<UEnum>())
		{
			std::vector<std::string> vals;
			auto names = ((UEnum*)prop)->GetNames();
			for (int i = 0; i < names.Count(); i++)
				vals.push_back(names.at(i)->GetName());
			Enums[prop->GetName()] = vals;
		}
		else if (prop->IsA("Core.State"))
			States.push_back(prop->GetName());
		else
			props.push_back(prop);
	}

	Properties.clear();
	Properties = props;
}

FunctionDef* ClassDef::GetFunctionByName(std::string funcName)
{
	for (auto &func : Functions)
		if (func.Name == funcName)
			return &func;

	return nullptr;
}