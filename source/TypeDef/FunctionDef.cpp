#include <TypeAggregator/TypeDef/FunctionDef.h>
#include <TypeAggregator/TypeDef/PropertyDef.h>
#include <TypeAggregator/Types/Core/UFunction.h>
#include <TypeAggregator/EnumHelpers.h>

using namespace TypeAggregator::TypeDef;
using namespace TypeAggregator::Types::Core;

FunctionDef::FunctionDef(TypeAggregator::Types::Core::UFunction* func)
{
	Name = func->GetName();
	ParentName = "";
	PackageName = "";
	ObjectInternalInteger = func->GetObjectInternalInteger();
	Pointer = func->GetPointer(); // TODO: replace with ProcessEvent for Events and CallFunction for non-native functions
	ReturnType = "void";


	FunctionFlags = func->GetFunctionFlagsList();//enum_flags_names<EFunctionFlags>(enum_cast<EFunctionFlags>(func->GetFunctionFlags()).value());

	for (auto& prop : func->GetProperties())
	{
		if (prop->HasPropertyFlag(EPropertyFlags::Parm))
		{
			if (prop->HasPropertyFlag(EPropertyFlags::ReturnParm))
			{
				ReturnType = prop->GetType();
				ReturnValueOffset = prop->GetOffset();
			}
			else
				Parameters.push_back((PropertyDef)prop);
		}
		else
			Locals.push_back((PropertyDef)prop);

		NumParams++;
		ParamSize += prop->GetPropertySize();
	}
}

PropertyDef* FunctionDef::GetParameterByName(std::string paramName)
{
	for (auto &param : Parameters)
		if (param.Name == paramName)
			return &param;

	return nullptr;
}

PropertyDef* FunctionDef::GetParameterByOffset(uint32_t offset)
{
	for (auto &param : Parameters)
		if (param.Offset == offset)
			return &param;

	return nullptr;
}

PropertyDef* FunctionDef::GetLocalByName(std::string localName)
{
	for (auto &local : Locals)
		if (local.Name == localName)
			return &local;

	return nullptr;
}

PropertyDef* FunctionDef::GetLocalByOffset(uint32_t offset)
{
	for (auto &local : Locals)
		if (local.Offset == offset)
			return &local;

	return nullptr;
}