#include <TypeAggregator/TypeDef/Loader.h>
#include <TypeAggregator/TypeDef/StructDef.h>
#include <TypeAggregator/TypeDef/ClassDef.h>
#include <regex>
#include <filesystem>
#include <spdlog/fmt/fmt.h>
#include <nlohmann/json.hpp>
#include <fstream>

using namespace TypeAggregator::TypeDef;
namespace fs = std::filesystem;
using json = nlohmann::json;

StructDef* TypeDefLoader::LoadDefinitionFile(std::string typeSlug)
{
	fs::path typeDefPath = fs::path(DefinitionFolder + "/" + std::regex_replace(typeSlug, std::regex("\\."), "/") + ".json");

	if (!std::filesystem::exists(typeDefPath))
		throw new std::runtime_error(fmt::format("Definition file {} does not exist!", typeDefPath.string()).c_str());

	json j = ParseJsonFile(typeDefPath);

	StructDef* def = nullptr;

	try {
		def = new ClassDef();
		*def = j.get<ClassDef>();
	}
	catch (json::exception const &ex) {
		throw new std::runtime_error(fmt::format("Definition file {} is invalid: {}", typeDefPath.string(), ex.what()).c_str());
	}

	return def;
}

std::vector<StructDef*> TypeDefLoader::LoadDefinitionFiles(std::initializer_list<std::string> typeSlugs)
{
	auto structDefs = std::vector<StructDef*>();

	for (auto typeSlug : typeSlugs)
		structDefs.push_back(LoadDefinitionFile(typeSlug));

	return structDefs;
}

json TypeDefLoader::ParseJsonFile(std::string filePath)
{
	json j;

	std::ifstream fileStream(filePath);
	fileStream >> j;
	fileStream.close();

	return j;
}