#include <TypeAggregator/TypeDef/PropertyDef.h>
#include <TypeAggregator/Types/Core/UProperty.h>
#include <TypeAggregator/EnumHelpers.h>

using namespace TypeAggregator::TypeDef;
using namespace TypeAggregator::Types::Core;

PropertyDef::PropertyDef(UProperty* prop)
{
	Name = prop->GetName();
	Offset = prop->GetOffset();
	Type = prop->GetType();
	PropertySize = prop->GetPropertySize();

	PropertyFlags = prop->GetPropertyFlagsList();
}
