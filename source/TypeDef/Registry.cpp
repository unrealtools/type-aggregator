#include <TypeAggregator/TypeDef/Registry.h>

#include <TypeAggregator/TypeDef/StructDef.h>
#include <TypeAggregator/Types/Core/UnrealNames.h>
#include <TypeAggregator/Types/Core/UObject.h>
#include <TypeAggregator/Types/Core/UStruct.h>
#include <TypeAggregator/Types/Core/UClass.h>
#include <TypeAggregator/Types/Core/UConst.h>
#include <TypeAggregator/Types/Core/UEnum.h>

using namespace TypeAggregator::TypeDef;
using namespace TypeAggregator::Types;

std::map<std::type_index, std::string> TypeRegistry::RequiredTypeDefinitions = {
	{typeid(Types::Core::FName), "Core.FName"},
	{typeid(Types::Core::FNameEntry), "Core.FNameEntry"},
	{typeid(Types::Core::UObject), "Core.UObject"},
	{typeid(Types::Core::UStruct), "Core.UStruct"},
	{typeid(Types::Core::UClass), "Core.UClass"},
	{typeid(Types::Core::UField), "Core.UField"},
	{typeid(Types::Core::UProperty), "Core.UProperty"},
	{typeid(Types::Core::UConst), "Core.UConst"},
	{typeid(Types::Core::UEnum), "Core.UEnum"}
};

void TypeRegistry::Initialize(TypeDefLoader* typeDefLoader)
{
	TypeRegistry::Loader = typeDefLoader;
}

StructDef* TypeRegistry::GetTypeDef(std::string typeName)
{
	if (StructDefinitions.find(typeName) == StructDefinitions.end())
		StructDefinitions[typeName] = Loader->LoadDefinitionFile(typeName);

	return StructDefinitions[typeName];
}

StructDef* TypeRegistry::GetTypeDef(int objectInternalInteger)
{
	for (auto& [typeName, strDef] : StructDefinitions)
		if (strDef->ObjectInternalInteger == objectInternalInteger)
			return strDef;

	return nullptr;
}

std::vector<StructDef*> TypeRegistry::GetTypeDefs()
{
	std::vector<StructDef*> strDefs = {};

	for (auto& [typeName, strDef] : StructDefinitions)
		strDefs.push_back(strDef);

	return strDefs;
}

void TypeRegistry::AddTypeDef(std::string typeName, StructDef* def)
{
	StructDefinitions[typeName] = def;
}