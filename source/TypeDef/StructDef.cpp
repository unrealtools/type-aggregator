#include <TypeAggregator/TypeDef/StructDef.h>
#include <TypeAggregator/Types/Core/UStruct.h>
#include <TypeAggregator/EnumHelpers.h>

using namespace TypeAggregator::TypeDef;
using namespace TypeAggregator::Types::Core;


StructDef::StructDef(UStruct* str)
{
	auto superField = str->GetSuperField();

	Name = str->GetNameCPP();
	ParentName = superField->GetAddress() != 0 ? superField->GetFullNameCPP() : "";
	PackageName = str->GetPackage();
	ObjectInternalInteger = str->GetObjectInternalInteger();
	Size = str->GetPropertySize();
	StructSize = superField->GetAddress() != 0 ? Size - UStruct(superField->GetAddress()).GetPropertySize() : Size;

	for (auto& prop : str->GetProperties())
		Properties.push_back((PropertyDef)prop);

	ObjectFlags = str->GetObjectFlagsList();//enum_flags_names<EObjectFlags>(enum_cast<EObjectFlags>(str->GetObjectFlags()).value());
}


PropertyDef* StructDef::GetPropertyByName(std::string propName)
{
	for (auto& prop : Properties)
		if (prop.Name == propName)
			return &prop;

	return nullptr;
}

PropertyDef* StructDef::GetPropertyByOffset(uint32_t offset)
{
	for (auto& prop : Properties)
		if (prop.Offset == offset)
			return &prop;

	return nullptr;
}

bool StructDef::HasProperty(std::string propertyName)
{
	for (auto& prop : Properties)
		if (prop.Name == propertyName)
			return true;

	return false;
}

bool StructDef::HasProperty(uint32_t propertyOffset)
{
	for (auto& prop : Properties)
		if (prop.Offset == propertyOffset)
			return true;
	
	return false;
}