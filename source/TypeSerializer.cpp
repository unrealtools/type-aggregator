#include <TypeAggregator/TypeSerializer.h>
#include <TypeAggregator/Types/Core/UClass.h>
#include <TypeAggregator/TypeDef/StructDef.h>
#include <TypeAggregator/TypeDef/ClassDef.h>

using namespace TypeAggregator;
using namespace TypeAggregator::Types::Core;


void Serializer::SerializeStruct(nlohmann::json& j, UStruct* str)
{
	TypeDef::StructDef def = str;
	j = def;
}

void Serializer::SerializeClass(nlohmann::json& j, UClass* cls)
{
	TypeDef::ClassDef def = cls;
	j = def;
}
