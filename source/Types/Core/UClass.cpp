#include <TypeAggregator/Types/Core/UClass.h>
#include <TypeAggregator/EnumHelpers.h>

using namespace TypeAggregator::Types::Core;

uint32_t UClass::GetClassFlags()
{
	return Read<uint32_t, UClass>("ClassFlags");
}

UClass* UClass::GetDefaultClass()
{
	return new UClass(Read<uintptr_t, UClass>("ClassDefaultObject"));
}

bool UClass::HasClassFlags(EClassFlags flags)
{
	return this->GetClassFlags() & magic_enum::enum_integer(flags);
}

std::vector<std::string> UClass::GetClassFlagsList()
{
	return enum_flags_names<EClassFlags>(magic_enum::enum_flags_cast<EClassFlags>(GetClassFlags()).value_or(EClassFlags::None));
}