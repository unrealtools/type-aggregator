#include <TypeAggregator/Types/Core/UConst.h>

using namespace TypeAggregator::Types::Core;

FString UConst::GetValue()
{
	return Read<FString, UConst>("Value");
}