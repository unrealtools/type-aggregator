#include <TypeAggregator/Types/Core/UEnum.h>

using namespace TypeAggregator;
using namespace TypeAggregator::Types::Core;

TUnrealTypeArray<FName> UEnum::GetNames()
{
	TUnrealTypeArray<FName> names = Address + GetPropertyDefByName<UEnum>("Names")->Offset;
	return names;
}