#include <TypeAggregator/Types/Core/UField.h>

using namespace TypeAggregator::Types::Core;

UField* UField::GetNext()
{
	return new UField(Read<uintptr_t, UField>("Next"));
}