#include <TypeAggregator/Types/Core/UFunction.h>
#include <TypeAggregator/EnumHelpers.h>
#define NOMINMAX
#include <Windows.h>
using namespace TypeAggregator::Types::Core;

uint32_t UFunction::GetFunctionFlags()
{
	return Read<uint32_t, UFunction>("FunctionFlags");
}

uint16_t UFunction::GetNativeIndex()
{
	return Read<uint16_t, UFunction>("NativeIndex");
}

uintptr_t UFunction::GetPointer()
{
	return Read<uintptr_t, UFunction>("Address") - (uintptr_t)GetModuleHandle(nullptr);
}

bool UFunction::HasFunctionFlag(EFunctionFlags flag)
{
	return GetFunctionFlags() & magic_enum::enum_integer(flag);
}

std::vector<std::string> UFunction::GetFunctionFlagsList()
{
	return enum_flags_names<EFunctionFlags>(magic_enum::enum_flags_cast<EFunctionFlags>(GetFunctionFlags()).value_or(EFunctionFlags::None));
}
