#include <TypeAggregator/Types/Core/UObject.h>
#include <TypeAggregator/Types/Core/UnrealNames.h>
#include <TypeAggregator/Types/Core/UClass.h>
#include <TypeAggregator/Types/GlobalStore.h>
#include <TypeAggregator/EnumHelpers.h>
#include <TypeAggregator/Types/Core/UEnum.h>

using namespace TypeAggregator;
using namespace TypeAggregator::Types::Core;

TUnrealTypeArray<UObject*>* GlobalObjects = nullptr;

uint32_t UObject::GetObjectInternalInteger()
{
    return Read<uint32_t, UObject>("ObjectInternalInteger");
}

uint32_t UObject::GetObjectFlags()
{
    return Read<uint32_t, UObject>("ObjectFlags");
}

std::string UObject::GetName()
{
    auto name = Read<FName, UObject>("Name");
    return name.GetName();
}

std::string UObject::GetFullName()
{
    std::string fullName = this->GetName();

    for (auto outer = GetOuter(); outer->GetAddress() != 0; outer = outer->GetOuter())
        fullName = outer->GetName() + "." + fullName;

    return fullName;
}

std::string UObject::GetNameCPP()
{
    static int objectClsIndex = FindObject("Core.Object")->GetObjectInternalInteger();
    static int actorClsIndex = FindObject("Engine.Actor")->GetObjectInternalInteger();

    if (this->IsA<UClass>())
    {
        for (auto superClass = (UClass*)this; superClass != nullptr && superClass->GetAddress() != 0; superClass = (UClass*)superClass->GetSuperField())
            if (superClass->GetObjectInternalInteger() == actorClsIndex)
                return std::string("A") + this->GetName();
            else if (superClass->GetObjectInternalInteger() == objectClsIndex)
                return std::string("U") + this->GetName();
    }
    else if (this->IsA<UEnum>())
    {
        return this->GetName();
    }

    return std::string("F") + this->GetName();
}

std::string UObject::GetFullNameCPP()
{
    return this->GetPackage() + "." + this->GetNameCPP();
}

std::string UObject::GetPackage()
{
    std::string outerName = "";
    UObject* outer;

    for (outer = GetOuter(); outer->GetAddress() != 0; outer = outer->GetOuter())
        outerName = outer->GetName();

    return outerName;
}

UObject* UObject::GetOuter()
{
    return Read<UObject*, UObject>("Outer");
}

UClass* UObject::GetClass()
{
    return Read<UClass*, UObject>("Class");
}

UObject* UObject::GetObjectArchetype()
{
    return Read<UObject*, UObject>("ObjectArchetype");
}

UObject* UObject::FindObject(std::string objectName)
{
    if (GlobalStore::ObjectCache.find(objectName) != GlobalStore::ObjectCache.end())
        return GlobalStore::ObjectCache[objectName];

    auto objs = GlobalStore::Objects();
    auto objCount = objs->Count();
    for (uint32_t i = 0; i < objCount; i++)
    {
        auto obj = objs->at(i);

        if (obj->GetAddress() != 0 && obj->GetFullName() == objectName)
        {
            GlobalStore::ObjectCache[objectName] = obj;
            return obj;
        }
    }

    GlobalStore::ObjectCache[objectName] = nullptr;
    return nullptr;
}

UClass* UObject::FindClass(std::string className)
{
    return (UClass*)FindObject(className);
}

bool UObject::IsA(UClass* cls)
{
    int clsIndex = cls->GetObjectInternalInteger();
    for (auto SuperClass = this->GetClass(); SuperClass->GetAddress() != 0; SuperClass = (UClass*)SuperClass->GetSuperField())
        if (SuperClass->GetObjectInternalInteger() == clsIndex)
            return true;

    return false;
}

bool UObject::IsA(std::string className)
{
    auto cls = FindClass(className);
    if (cls == nullptr || cls->GetAddress() == 0)
        return false;
    return IsA(cls);
}

bool UObject::HasObjectFlags(EObjectFlags flags)
{
    return this->GetObjectFlags() & magic_enum::enum_integer(flags);
}

std::vector<std::string> UObject::GetObjectFlagsList()
{
    return enum_flags_names<EObjectFlags>(magic_enum::enum_flags_cast<EObjectFlags>(GetObjectFlags()).value_or(EObjectFlags::NoFlags));
}
