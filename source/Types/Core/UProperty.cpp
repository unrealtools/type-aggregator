#include <TypeAggregator/Types/Core/UProperty.h>
#include <TypeAggregator/Types/Core/UClass.h>
#include <TypeAggregator/EnumHelpers.h>
#include <spdlog/spdlog.h>
#include <TypeAggregator/Types/Core/FString.h>
#include <TypeAggregator/Types/Core/UnrealNames.h>

using namespace TypeAggregator::Types::Core;

uint32_t UProperty::GetOffset()
{
	return Read<uint32_t, UProperty>("Offset");
}

uint32_t UProperty::GetPropertyFlags()
{
	return Read<uint32_t, UProperty>("PropertyFlags");
}

uint32_t UProperty::GetPropertySize()
{
	return Read<uint32_t, UProperty>("PropertySize");
}

std::string UProperty::GetDefaultValue()
{
	auto cls = UClass(this->GetOuter()->GetAddress());
	auto defaultCls = cls.GetDefaultClass();

	uintptr_t defaultAddr = defaultCls->GetAddress() + this->GetOffset();

	if (this->IsA("Core.ByteProperty"))
		return fmt::format("0x{:X}", *(unsigned char*)defaultAddr);
	else if (this->IsA("Core.IntProperty"))
		return fmt::format("{}", *(int*)defaultAddr);
	else if (this->IsA("Core.FloatProperty"))
		return fmt::format("{}", *(float*)defaultAddr);
	else if (this->IsA("Core.BoolProperty"))
		return fmt::format("{}", *(bool*)defaultAddr);
	else if (this->IsA("Core.StrProperty"))
		return (*(FString*)defaultAddr).ToChar();
	else if (this->IsA("Core.NameProperty"))
		return (*(FName*)defaultAddr).GetName();
	else if (this->IsA("Core.DelegateProperty"))
		return "Core.FScriptDelegate";
	else if (this->IsA("Core.ObjectProperty") || this->IsA("Core.ClassProperty") || this->IsA("Core.InterfaceProperty") || this->IsA("Core.StructProperty"))
		return UObject(*(uintptr_t*)defaultAddr).GetFullName();
	else if (this->IsA("Core.ArrayProperty"))
	{
		/*auto inner = this->Read<UProperty*>("Core.ArrayProperty", "InnerProperty");
		if (inner->GetAddress() == 0)
		{
			spdlog::error("Failed to get InnerProperty of {}", this->GetFullName());
			return "Core.TArray<void*>";
		}

		return fmt::format("Core.TArray<{}>", inner->GetType());*/
		return "NotImpl";
	}
	else if (this->IsA("Core.MapProperty"))
	{
		return "NotImpl";
	}

	spdlog::error("Failed to get property default value for {}", this->GetFullName());
	return "void*";
}

std::string UProperty::GetType()
{
	if (this->IsA("Core.ByteProperty"))
	{
		auto en = this->Read<UClass*>("Core.ByteProperty", "Enum");
		if (en->GetAddress() == 0)
		{
			spdlog::error("Failed to get Enum for {}", this->GetFullName());
			return "unsigned char";
		}

		return en->GetFullNameCPP();
	}
		//return "unsigned char";
	else if (this->IsA("Core.IntProperty"))
		return "int";
	else if (this->IsA("Core.FloatProperty"))
		return "float";
	else if (this->IsA("Core.BoolProperty"))
		return "bool";
	else if (this->IsA("Core.StrProperty"))
		return "Core.FString";
	/*else if (this->IsA("Core.StringRefProperty"))
		return "void*";*/
	else if (this->IsA("Core.NameProperty"))
		return "Core.FName";
	else if (this->IsA("Core.DelegateProperty"))
		return "Core.FScriptDelegate";
	else if (this->IsA("Core.ObjectProperty"))
	{
		auto cls = this->Read<UClass*>("Core.ObjectProperty", "PropertyClass");
		if (cls->GetAddress() == 0)
		{
			spdlog::error("Failed to get PropertyClass for {}", this->GetFullName());
			return "void*";
		}
		
		return cls->GetFullNameCPP() + "*";
	}
	else if (this->IsA("Core.ClassProperty"))
	{
		auto cls = this->Read<UClass*>("Core.ClassProperty", "MetaClass");
		if (cls->GetAddress() == 0)
		{
			spdlog::error("Failed to get MetClass for {}", this->GetFullName());
			return "void*";
		}

		return cls->GetFullNameCPP() + "*";
	}
	else if (this->IsA("Core.InterfaceProperty"))
	{
		auto cls = this->Read<UClass*>("Core.InterfaceProperty", "InterfaceClass");
		if (cls->GetAddress() == 0)
		{
			spdlog::error("Failed to get InterfaceClass for {}", this->GetFullName());
			return "void*";
		}

		return cls->GetFullNameCPP() + "*";
	}
	else if (this->IsA("Core.StructProperty"))
	{
		auto str = this->Read<UStruct*>("Core.StructProperty", "PropertyStruct");
		if (str->GetAddress() == 0)
		{
			spdlog::error("Failed to get PropertyStruct for {}", this->GetFullName());
			return "void*";
		}

		return str->GetFullNameCPP();
	}
	else if (this->IsA("Core.ArrayProperty"))
	{
		auto inner = this->Read<UProperty*>("Core.ArrayProperty", "InnerProperty");
		if (inner->GetAddress() == 0)
		{
			spdlog::error("Failed to get InnerProperty of {}", this->GetFullName());
			return "Core.TArray<void*>";
		}

		return fmt::format("Core.TArray<{}>", inner->GetType());
	}
	else if (this->IsA("Core.MapProperty"))
	{
		auto key = this->Read<UProperty*>("Core.MapProperty", "Key");
		auto val = this->Read<UProperty*>("Core.MapProperty", "Value");

		if (key->GetAddress() == 0 || val->GetAddress() == 0)
		{
			spdlog::error("Failed to get Key/Value properties from {}", this->GetFullName());
			return "Core.TMap<void*, void*>";
		}

		return fmt::format("Core.TMap<{}, {}>", key->GetType(), val->GetType());
	}

	spdlog::error("Failed to get property type for {}", this->GetFullName());
	return "void*";
}

bool UProperty::HasPropertyFlag(EPropertyFlags flag)
{
	return this->GetPropertyFlags() & magic_enum::enum_integer(flag);
}

std::vector<std::string> UProperty::GetPropertyFlagsList()
{
	return enum_flags_names<EPropertyFlags>(magic_enum::enum_flags_cast<EPropertyFlags>(GetPropertyFlags()).value_or(EPropertyFlags::None));
}
