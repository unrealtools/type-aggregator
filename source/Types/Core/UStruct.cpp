#include <TypeAggregator/Types/Core/UStruct.h>
#include <TypeAggregator/Types/Core/UClass.h>

using namespace TypeAggregator::Types::Core;

UField* UStruct::GetSuperField()
{
	return new UField(Read<uintptr_t, UStruct>("SuperField"));
}

UField* UStruct::GetChildren()
{
	return new UField(Read<uintptr_t, UStruct>("Children"));
}

uint32_t UStruct::GetPropertySize()
{
	return Read<uint32_t, UStruct>("PropertySize");
}

std::vector<UProperty*> UStruct::GetProperties()
{
	auto props = std::vector<UProperty*>();

	for (auto prop = (UProperty*)this->GetChildren(); prop->GetAddress() != 0; prop = (UProperty*)prop->GetNext())
	{
		if (
			!prop->IsA("Core.Function")
			&& !prop->IsA("Core.Const")
			&& !prop->IsA("Core.Enum")
			&& !prop->IsA("Core.ScriptStruct")
			)
			props.push_back(prop);
	}

	return props;
}