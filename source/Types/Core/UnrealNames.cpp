#include <TypeAggregator/Types/Core/UnrealNames.h>
#include <TypeAggregator/Types/GlobalStore.h>

using namespace TypeAggregator;
using namespace TypeAggregator::Types::Core;

std::string FName::GetName()
{
    uint32_t index = GetIndex();
    auto entry = GlobalStore::Names()->at(index);
    return entry->GetName();
}