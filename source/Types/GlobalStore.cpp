#include <TypeAggregator/Types/GlobalStore.h>

using namespace TypeAggregator;
using namespace TypeAggregator::Types;
using namespace TypeAggregator::Types::Core;

TUnrealTypeArray<FNameEntry*, uint32_t>* GlobalStore::Names()
{
	if (!GNames)
		GNames = new TUnrealTypeArray<FNameEntry*, uint32_t>();
	
	return GNames;
}

TUnrealTypeArray<UObject*, uint32_t>* GlobalStore::Objects()
{
	if (!GObjects)
		GObjects = new TUnrealTypeArray<UObject*, uint32_t>();

	return GObjects;
}