#pragma once

#include <TypeAggregator/TUnrealTypeArray.h>
#include <TypeAggregator/Types/Core/UnrealNames.h>
#include <TypeAggregator/Types/Core/UObject.h>

namespace TypeAggregator::Types
{
	class GlobalStore
	{
	public:
		static TypeAggregator::TUnrealTypeArray<Core::FNameEntry*>* Names();
		static TypeAggregator::TUnrealTypeArray<Core::UObject*>* Objects();
	};
}
