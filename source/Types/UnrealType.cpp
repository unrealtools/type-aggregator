#include <TypeAggregator/Types/UnrealType.h>
#include <TypeAggregator/TypeDef/Registry.h>
#include <spdlog/fmt/fmt.h>

using namespace TypeAggregator::Types;
using namespace TypeAggregator::TypeDef;

#ifdef _WIN32
#define NOMINMAX
#include <Windows.h>
#include <Psapi.h>
#include <iostream>

uintptr_t UnrealType::CheckAddressSpace(uintptr_t address)
{
	MEMORY_BASIC_INFORMATION mbi;
	VirtualQuery((LPVOID)address, &mbi, sizeof(mbi));
	if (address < (uintptr_t)mbi.BaseAddress)
		throw std::out_of_range(fmt::format("Memory not in range for {:X}", address));

	return (uintptr_t)mbi.BaseAddress + mbi.RegionSize;
}
#elif __linux__
#include <procmap/MemoryMap.hpp>
#include <algorithm>

uintptr_t UnrealType::CheckAddressSpace(uintptr_t address)
{
	procmap::MemoryMap mmap;
	auto segIt = std::find(mmap.begin(), mmap.end(), [&](procmap::MemorySegment seg) {
		return address > (uintptr_t)seg.startAddress() && address < (uintptr_t)seg.endAddress();
		});

	if (segIt == mmap.end())
		throw std::out_of_range(fmt::format("Memory not in range for {:X}", address));

	return (uintptr_t)(*segIt).endAddress();
}
#endif