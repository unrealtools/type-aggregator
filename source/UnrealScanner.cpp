#include <TypeAggregator/UnrealScanner.h>
#include <TypeAggregator/Types/Core/PropertyFlags.h>
#include <TypeAggregator/Types/Core/ClassFlags.h>
#include <TypeAggregator/Types/Core/FunctionFlags.h>
#include <TypeAggregator/Types/Core/UObject.h>

using namespace TypeAggregator;

#define OFFSET_OR_THROW(offName, offVar) \
	if(offVar > 0) \
		spdlog::debug("Found {} at {}", offName, offVar); \
	else \
		throw offset_exception(offName);

#pragma region property offset finder
int UnrealScanner::FindTArrayIndexOffset(uintptr_t* arrayData, bool isObjectArray, size_t startOffset, size_t maxOffset, size_t maxIndex)
{
	if (indexPropOffset > 0)
		return indexPropOffset;

	for (int i = startOffset; i < maxOffset; i++)
	{
		// iterate through objects to compare index offset
		for (int x = 0; x <= maxIndex; x++)
		{
			// check if next entry is valid object
			if (isObjectArray && !IsValidObjectPointer(arrayData[x + 1]))
				return -1;

			// get index of current and next object
			int currIndex = *(int*)(arrayData[x] + i);
			if (i == 0 && currIndex != 0 && currIndex != 1)
				break;

			int nextIndex = *(int*)(arrayData[x + 1] + i);

			// if indices doesnt accumulate skip to next offset
			if (nextIndex != currIndex + 1)
				break;

			// found the index offset
			if (x == maxIndex)
			{
				spdlog::debug("Found ArrayIndexOffset for {:X} at {}", (uintptr_t)arrayData, i);
				indexPropOffset = i;
				return i;
			}
		}
	}

	spdlog::error("Failed to find ArrayIndexOffset for {:X}", (uintptr_t)arrayData);
	return -1;
}

int UnrealScanner::FindObjectInternalIntegerOffset(uintptr_t* GObjects, size_t startOffset, size_t maxOffset, size_t maxObjects)
{
	if (indexPropOffset > 0)
		return indexPropOffset;

	for (int i = startOffset; i < maxOffset; i++)
	{
		// iterate through objects to compare index offset
		for (int x = 0; x <= maxObjects; x++)
		{
			// check if next entry is valid object
			if (!IsValidObjectPointer(GObjects[x + 1]))
				return -1;

			// get index of current and next object
			int currIndex = *(int*)(GObjects[x] + i);
			if (i == 0 && currIndex != 0 && currIndex != 1)
				break;

			int nextIndex = *(int*)(GObjects[x + 1] + i);

			// if indices doesnt accumulate skip to next offset
			if (nextIndex != currIndex + 1)
				break;

			// found the index offset
			if (x == maxObjects)
			{
				spdlog::debug("Found UObject::InternalInteger at {}", i);
				indexPropOffset = i;
				return i;
			}
		}
	}

	spdlog::debug("Failed to find UObject::InternalInteger in {:X}", (uintptr_t)GObjects);
	return -1;
}

int UnrealScanner::FindObjectNameOffset()
{
	int objectNameIndex = FindNameIndex("Object");
	int classNameIndex = FindNameIndex("Class");

	auto objects = *GlobalObjects;
	int objectNameOffset = 0;

	// iterate through objects to find UObject
	for (auto& obj : objects)
	{
		if (obj == 0)
			continue;

		// check if objectNameIndex is found in current object
		if ((objectNameOffset = MemSearch<int>(objectNameIndex, obj, 100, objectNameOffset + sizeof(int))) != 0)
		{
			// iterate through objects to find UClass
			for (auto& obj2 : objects)
			{
				if (obj2 == 0)
					continue;

				// check if classNameIndex is at same offset as objectNameIndex
				if (MemSearch<int>(classNameIndex, obj2, 100) == objectNameOffset)
				{
					spdlog::debug("Found UObject::Name at {}", objectNameOffset);
					namePropOffset = objectNameOffset;
					return objectNameOffset;
				}
			}
		}
	}

	throw offset_exception("UObject::Name");
	return -1;
}

int UnrealScanner::FindObjectOuterOffset()
{
	if (objectOuterOffset > 0)
		return objectOuterOffset;

	int objectIndex = FindObjectInternalInteger("Object");
	int classIndex = FindObjectInternalInteger("Class");
	int coreIndex = FindObjectInternalInteger("Core");

	uintptr_t objectAddr = GlobalObjects->at(objectIndex);
	uintptr_t classAddr = GlobalObjects->at(classIndex);
	uintptr_t coreAddr = GlobalObjects->at(coreIndex);

	objectOuterOffset = FindCommonOffset({ objectAddr, classAddr }, { coreAddr, coreAddr }, 100);

	OFFSET_OR_THROW("UObject::Outer", objectOuterOffset);
	return objectOuterOffset;
}

int UnrealScanner::FindObjectClassOffset()
{
	int objectIndex = FindObjectInternalInteger("Object");
	int classIndex = FindObjectInternalInteger("Class");

	uintptr_t objectAddr = GlobalObjects->at(objectIndex);
	uintptr_t classAddr = GlobalObjects->at(classIndex);

	int classOffset = FindCommonOffset({ objectAddr, classAddr }, { classAddr, classAddr }, 100);
	
	OFFSET_OR_THROW("UObject::Class", classOffset);
	return classOffset;
}

int UnrealScanner::FindStructSuperFieldOffset()
{
	int classIndex = FindObjectInternalInteger("Class");
	int stateIndex = FindObjectInternalInteger("State");
	int structIndex = FindObjectInternalInteger("Struct");

	uintptr_t classAddr = GlobalObjects->at(classIndex);
	uintptr_t stateAddr = GlobalObjects->at(stateIndex);
	uintptr_t structAddr = GlobalObjects->at(structIndex);

	int strSuperFieldOffset =  FindCommonOffset({ classAddr, stateAddr }, { stateAddr, structAddr }, 200);
	
	OFFSET_OR_THROW("UStruct::SuperField", strSuperFieldOffset);
	return strSuperFieldOffset;
}

int UnrealScanner::FindStructPropSizeOffset()
{
	structPropSizeOffset = FindCommonObjectOffset(
		{ "Core.Object.Pointer", "Core.Object.Guid", "Core.Object.Vector" },
		{ (int)sizeof(int), (int)sizeof(int) * 4, (int)sizeof(float) * 3 }, 
		200);

	return structPropSizeOffset;
}

int UnrealScanner::FindStructChildOffset()
{
	static int structChildOffset = -1;
	if (structChildOffset > 0)
		return structChildOffset;

	int pointerIndex = FindObjectInternalInteger("Pointer");
	int dummyIndex = FindObjectInternalInteger("Dummy");

	uintptr_t pointerAddr = GlobalObjects->at(pointerIndex);
	uintptr_t dummyAddr = GlobalObjects->at(dummyIndex);

	structChildOffset = MemSearch<uintptr_t>(dummyAddr, pointerAddr, 100);

	OFFSET_OR_THROW("UStruct::Children", structChildOffset);
	return structChildOffset == 0 ? -1 : structChildOffset;
}

int UnrealScanner::FindFieldNextOffset()
{
	int aNameIndex = FindNameIndex("A");
	int bNameIndex = FindNameIndex("B");

	int guidObjectIndex = FindObjectInternalInteger("Guid");

	int guidBObjectIndex = FindFullNameIndex("Core.Object.Guid.B", FindObjectOuterOffset());

	uintptr_t guidAddr = GlobalObjects->at(guidObjectIndex);
	uintptr_t bAddr = GlobalObjects->at(guidBObjectIndex);

	uintptr_t guidChild = *(uintptr_t*)(guidAddr + FindStructChildOffset());

	int nextOffset = -1;
	if (aNameIndex == *(int*)(guidChild + namePropOffset))
		nextOffset = MemSearch<uintptr_t>(bAddr, guidChild, 0x60);

	OFFSET_OR_THROW("UField::Next", nextOffset);
	return nextOffset;
}

int UnrealScanner::FindPropertySizeOffset(size_t childOffset, size_t nextOffset)
{
	int boneAtomIndex = FindFullNameIndex("Core.Object.BoneAtom");
	uintptr_t boneAtomAddr = GlobalObjects->at(boneAtomIndex);

	uintptr_t boneAtomRotAddr = *(uintptr_t*)(boneAtomAddr + childOffset);
	uintptr_t boneAtomTransAddr = *(uintptr_t*)(boneAtomRotAddr + nextOffset);
	uintptr_t boneAtomScaleAddr = *(uintptr_t*)(boneAtomTransAddr + nextOffset);

	int propSizeOff = FindCommonOffset(
		{ boneAtomRotAddr, boneAtomTransAddr, boneAtomScaleAddr },
		{ sizeof(float) * 4, sizeof(float) * 3, sizeof(float) },
		0x120
	);

	OFFSET_OR_THROW("UStruct::PropertySize", propSizeOff);
	return propSizeOff;
}

int UnrealScanner::FindPropertyOffsetOffset()
{
	int propOffOffste = FindCommonObjectOffset<size_t>(
		{"Core.Object.Guid.A", "Core.Object.Guid.B", "Core.Object.Guid.C", "Core.Object.Guid.D"},
		{0, sizeof(int), sizeof(int) * 2, sizeof(int) * 3},
		0x120
	);

	OFFSET_OR_THROW("UProperty::Offset", propOffOffste);
	return propOffOffste;
}

int UnrealScanner::FindObjectPropertyClassOffset()
{
	int objectIndex = FindFullNameIndex("Core.Object");
	uintptr_t obj = GlobalObjects->at(objectIndex);
	int objectSize = *(int*)(obj + structPropSizeOffset);

	int objectOuterIndex = FindFullNameIndex("Core.Object.Outer");
	uintptr_t objOuter = GlobalObjects->at(objectOuterIndex);

	int objPropClassOff = MemSearch<uintptr_t>(obj, objOuter, 0x150, objectSize);
	
	OFFSET_OR_THROW("UObjectProperty::PropertyClass", objPropClassOff);
	return objPropClassOff;
}

int UnrealScanner::FindClassPropertyClassOffset()
{
	int clsIndex = FindFullNameIndex("Core.Class");
	int objClassPropIndex = FindFullNameIndex("Core.Object.Class");
	int objIndex = FindFullNameIndex("Core.Object");

	uintptr_t obj = GlobalObjects->at(objIndex);
	uintptr_t cls = GlobalObjects->at(clsIndex);
	uintptr_t objClsProp = GlobalObjects->at(objClassPropIndex);

	int objSize = *(int*)(obj + structPropSizeOffset);

	int classPropClassOff = MemSearch<uintptr_t>(obj, objClsProp, 0x150, objSize);

	OFFSET_OR_THROW("UClassProperty::MetaClass", classPropClassOff);
	return classPropClassOff;
}

int UnrealScanner::FindStructPropertyStructOffset()
{
	int vectorStructIndex = FindFullNameIndex("Core.Object.Vector");
	int headingAngleDirStrIndex = FindFullNameIndex("Core.Object.GetHeadingAngle.Dir");

	uintptr_t vectorStruct = GlobalObjects->at(vectorStructIndex);
	uintptr_t headingAngleDir = GlobalObjects->at(headingAngleDirStrIndex);

	int strPropStrOff = MemSearch<uintptr_t>(vectorStruct, headingAngleDir, 0x100);

	OFFSET_OR_THROW("UStructProperty::Struct", strPropStrOff);
	return strPropStrOff;
}

int UnrealScanner::FindArrayPropertyInnerPropOffset()
{
	int splitStrResIndex = FindFullNameIndex("Core.Object.SplitString.Result");
	int splitStrResPropIndex = FindFullNameIndex("Core.Object.SplitString.Result.Result");

	uintptr_t splitStrRes = GlobalObjects->at(splitStrResIndex);
	uintptr_t splitStrResProp = GlobalObjects->at(splitStrResPropIndex);

	int arrPropInnerOff = MemSearch<uintptr_t>(splitStrResProp, splitStrRes, 0x150);

	OFFSET_OR_THROW("UArrayProperty::InnerProperty", arrPropInnerOff);
	return arrPropInnerOff;
}

int UnrealScanner::FindPropertyFlagsOffset()
{
	using namespace TypeAggregator::Types::Core;

	int vTablePropIndex = FindFullNameIndex("Core.Object.VfTableObject");
	int objectFlagsPropIndex = FindFullNameIndex("Core.Object.ObjectFlags");

	uintptr_t vTable = GlobalObjects->at(vTablePropIndex);
	uintptr_t objectFlags = GlobalObjects->at(objectFlagsPropIndex);

	int fieldSize = GetStructSize("Core.Field");

	for (int i = fieldSize; i < fieldSize + 0x50; i++)
	{
		auto vtFlags = *(uint32_t*)(vTable + i);
		auto ofFlags = *(uint32_t*)(objectFlags + i);

		if ((vtFlags & (uint32_t)EPropertyFlags::Const) &&
			(vtFlags & (uint32_t)EPropertyFlags::Native) &&
			(vtFlags & (uint32_t)EPropertyFlags::NoExport) &&
			(ofFlags & (uint32_t)EPropertyFlags::Const) &&
			(ofFlags & (uint32_t)EPropertyFlags::Native))
		{
			spdlog::debug("Found UProperty::PropertyFlags at {}", i);
			return i;
		}
	}

	throw offset_exception("UProperty::PropertyFlags");
	return -1;
}

int UnrealScanner::FindClassFlagsOffset()
{
	using namespace TypeAggregator::Types::Core;

	int stateSize = GetStructSize("Core.State");

	int objectIndex = FindFullNameIndex("Core.Function");
	int propertyIndex = FindFullNameIndex("Core.Property");

	uintptr_t object = GlobalObjects->at(objectIndex);
	uintptr_t property = GlobalObjects->at(propertyIndex);

	for (int i = stateSize; i < stateSize + 0x50; i++)
	{
		auto objFlags = *(uint32_t*)(object + i);
		auto propFlags = *(uint32_t*)(property + i);
		if ((objFlags & (uint32_t)EClassFlags::IsAUFunction) &&
			(propFlags & (uint32_t)EClassFlags::IsAUProperty))
		{
			spdlog::debug("Found UClass::ClassFlags at {}", i);
			return i;
		}
	}

	throw offset_exception("UClass::ClassFlags");
	return -1;
}

int UnrealScanner::FindClassDefaultObjectOffset()
{
	auto uObjSize = GetStructSize("Core.State");

	for (auto objectPtr : *GlobalObjects)
	{
		if (objectPtr == 0)
			continue;

		auto obj = Types::Core::UObject(objectPtr);
		if (obj.IsA("Core.Class"))
		{
			auto defaultObjIndex = FindObjectInternalInteger("Default__" + obj.GetName());
			if (defaultObjIndex <= 0)
				continue;
			
			auto defaultObj = GlobalObjects->at(defaultObjIndex);
			if (defaultObj <= 0)
				continue;

			int classDefaultObjOffset = MemSearch<uintptr_t>(defaultObj, objectPtr, 0x250, uObjSize);
			return classDefaultObjOffset;
		}
	}

	return -1;
}

int UnrealScanner::FindFunctionFlagsOffset()
{
	using namespace TypeAggregator::Types::Core;

	int getSystemTimeIndex = FindFullNameIndex("Core.Object.GetSystemTime");
	int getAngDistIndex = FindFullNameIndex("Core.Object.GetAngularDistance");

	uintptr_t getSystemTime = GlobalObjects->at(getSystemTimeIndex);
	uintptr_t getAngularDistance = GlobalObjects->at(getAngDistIndex);

	int structSize = GetStructSize("Core.Struct");

	for (int i = structSize; i < structSize + 0x50; i++)
	{
		uint32_t gstFlags = *(uint32_t*)(getSystemTime + i);
		uint32_t gevFlags = *(uint32_t*)(getAngularDistance + i);

		if ((gstFlags & (uint32_t)EFunctionFlags::Final) &&
			(gstFlags & (uint32_t)EFunctionFlags::Native) &&
			(gevFlags & (uint32_t)EFunctionFlags::Final) &&
			(gevFlags & (uint32_t)EFunctionFlags::Native))
		{
			spdlog::debug("Found UFunction::FunctionFlags at {}", i);
			return i;
		}
	}

	throw offset_exception("UFunction::FunctionFlags");
	return -1;
}

int UnrealScanner::FindConstValueOffset()
{
	int maxInstConstIndex = FindFullNameIndex("Core.Object.MaxInt");
	uintptr_t maxInstConst = GlobalObjects->at(maxInstConstIndex);

	int fieldSize = GetStructSize("Core.Field");

	for (int i = fieldSize; i < fieldSize + 0x50; i++)
	{
		int count = *(int*)(maxInstConst + i);
		int max = *(int*)(maxInstConst + i + sizeof(int));

		if (count == 11 && max == 11)
		{
			spdlog::debug("Found UConst::Value at {}", i - sizeof(uintptr_t));
			return i - sizeof(uintptr_t);
		}
	}

	throw offset_exception("UConst::Value");
	return -1;
}

int UnrealScanner::FindEnumNamesOffset()
{
	int axisEnumIndex = FindFullNameIndex("Core.Object.EAxis");
	uintptr_t axisEnum = GlobalObjects->at(axisEnumIndex);

	int fieldSize = GetStructSize("Core.Field");

	for (int i = fieldSize; i < fieldSize + 0x50; i++)
	{
		int count = *(int*)(axisEnum + i);
		int max = *(int*)(axisEnum + i + sizeof(int));

		if (count == 6 && max == 6)
		{
			spdlog::debug("Found UEnum::Names at {}", i - sizeof(uintptr_t));
			return i - sizeof(uintptr_t);
		}
	}

	throw offset_exception("UEnum::Names");
	return -1;
}

int UnrealScanner::FindFunctionNativeIndexOffset()
{
	return 0;
}

int UnrealScanner::FindFunctionAddressOffset()
{
	int beginStateIndex = FindFullNameIndex("Core.Object.BeginState");
	uintptr_t beginState = GlobalObjects->at(beginStateIndex);

	int structSize = GetStructSize("Core.Struct");

	for (int i = structSize; i < structSize + 0x50; i++)
	{
		uintptr_t ptr = *(uintptr_t*)(beginState + i);

		for (int x = ptr - 1; x >= ptr - 0x50; x--)
		{
			if (!IsValidAddress(x))
				break;

			if (*(char*)(x) == (char)0xCC)
				continue;

			if (*(char*)(x - 2) != (char)0xC2 &&
				*(char*)(x - 2) != (char)0xCA &&
				*(char*)(x) != (char)0xC3 &&
				*(char*)(x) != (char)0xCB)
				break;
			
			spdlog::debug("Found UFunction::Address at {}", i);
			return i;
		}
	}

	throw offset_exception("UFunction::Address");
	return -1;
}
#pragma endregion

#pragma region global store finder
int UnrealScanner::FindGlobalNames()
{
	auto mem = Memory::instance();

	std::vector<Memory::ModuleInfo> modInfos = {};
	mem->getModuleInfoOfAllModules(&modInfos);

	uintptr_t gNames = 0;
	size_t searchOffset = 0;

	while (true)
	{
		// find address of "None"
		uintptr_t noneAddr = mem->findPattern(&modInfos, (char*)nonePattern, (char*)"xxxxx", searchOffset);
		if (noneAddr == 0)
			break;

		// find address of "ByteProperty"
		uintptr_t byteAddr = mem->findPattern(&modInfos, (char*)bytePropPattern, (char*)"xxxxxxxxxxxxx", noneAddr);
		if (byteAddr == 0)
			break;

		// find address of "IntProperty"
		uintptr_t intAddr = mem->findPattern(&modInfos, (char*)intPropPattern, (char*)"xxxxxxxxxxxx", byteAddr);
		if (intAddr == 0)
			break;

		// calc distances of noneAddr, byteAddr and intAddr
		size_t diffNoneByte = byteAddr - noneAddr - sizeof(nonePattern);
		size_t diffIntByte = intAddr - byteAddr - sizeof(bytePropPattern);

		// if distances are the same its probably the gnames table
		if (diffNoneByte == diffIntByte)
		{
			// calc the FNameEntry base addresses
			uintptr_t noneEntryAddr = noneAddr - diffNoneByte;
			uintptr_t byteEntryAddr = byteAddr - diffNoneByte;
			uintptr_t intEntryAddr = intAddr - diffNoneByte;

			spdlog::debug("Found GlobalNames chunks at 0x{:X} (diff: 0x{:X})", noneEntryAddr, diffNoneByte);

			// find memory containing the addresses as chunks
			uintptr_t namesChunk = 0;
			for (auto i = modInfos.rbegin(); i != modInfos.rend(); ++i)
			{
				auto addr = (*i).modBase;
				if (*(uintptr_t*)(addr) != noneEntryAddr)
					continue;
				if (*(uintptr_t*)(addr + sizeof(uintptr_t)) != byteEntryAddr)
					continue;
				if (*(uintptr_t*)(addr + (sizeof(uintptr_t) * 2)) != intEntryAddr)
					continue;

				namesChunk = addr;
				break;
			}

			if (namesChunk != 0)
			{
				spdlog::debug("Found GlobalNames data at 0x{:X}", namesChunk);
				// find tarray for memory chunk
				uintptr_t namesArrayPtr = 0;
				do {
					namesArrayPtr = mem->findValue<uintptr_t>(&modInfos, namesChunk, namesArrayPtr);

					int arrCount = *(int*)(namesArrayPtr + sizeof(uintptr_t));
					int arrMax = *(int*)(namesArrayPtr + sizeof(uintptr_t) + sizeof(int));

					if (arrCount > 1000 && arrMax > 1000 && arrCount < arrMax)
					{
						GlobalNames = reinterpret_cast<TArray<uintptr_t>*>(namesArrayPtr);
						FNameEntryNameOffset = diffNoneByte;
						spdlog::debug("Found GlobalNames at 0x{:X}", namesArrayPtr);
						return namesArrayPtr;
					}

					namesArrayPtr += sizeof(uintptr_t);
				} while (namesArrayPtr != 0);
			}
		}

		if (diffNoneByte > 0x1000)
			searchOffset = byteAddr - 0x1000;
		else
			searchOffset = noneAddr + sizeof(nonePattern);
	}

	throw offset_exception("GlobalNames");
	return -1;
}

int UnrealScanner::FindGlobalNamesByPattern()
{
	auto mem = Memory::instance();

	Memory::ModuleInfo moduleInfo = {};
	if (!mem->getModuleInfo(GetModuleHandleA(nullptr), &moduleInfo))
		return -1;

	uintptr_t gNamesRef = mem->findPattern(&moduleInfo, (char*)globalNamesPattern, (char*)globalNamesMask);

	if (gNamesRef > 0)
	{
		uintptr_t gNames = *(uintptr_t*)(gNamesRef + 2);
		GlobalNames = (TArray<uintptr_t>*)gNames;

		uintptr_t firstName = GlobalNames->at(0);

		FNameEntryNameOffset = MemSearch<char>(0x4E, firstName, 0x50);

		spdlog::info("Found GlobalNames by pattern at 0x{:X}", gNames);
		return gNames;
	}

	return -1;
}

int UnrealScanner::FindGlobalObjects(int GNames)
{
	MEMORY_BASIC_INFORMATION memInfo;
	if (VirtualQuery((PVOID)GNames, &memInfo, sizeof(memInfo)) != sizeof(memInfo))
		return 0;

	// GlobalObjects must be in the same module address space
	uintptr_t offset = (uintptr_t)memInfo.BaseAddress;
	uintptr_t max = (uintptr_t)memInfo.BaseAddress + memInfo.RegionSize;


	for (uintptr_t i = offset; i < max - (sizeof(int) * 2); i++)
	{
		int arrCount = *(int*)(i);
		int arrMax = *(int*)(i + sizeof(int));

		// possible TArray
		if (arrCount > 1000 && arrMax > 1000 && arrCount < arrMax)
		{
			// check if is valid global object array
			if (ValidateGlobalObjectsArray(i - sizeof(uintptr_t)) > 0)
			{
				spdlog::debug("Found GlobalObjects at 0x{:X}", i - sizeof(uintptr_t));
				GlobalObjects = reinterpret_cast<TArray<uintptr_t>*>(i - sizeof(uintptr_t));
				return (i - sizeof(uintptr_t));
			}
		}
	}

	throw offset_exception("GlobalObjects");
	return -1;
}

int UnrealScanner::FindGlobalObjectsByPattern()
{
	auto mem = Memory::instance();
	
	Memory::ModuleInfo moduleInfo = {};
	if (!mem->getModuleInfo(GetModuleHandleA(nullptr), &moduleInfo))
		return -1;

	uintptr_t gObjectsRef = mem->findPattern(&moduleInfo, (char*)globalObjectsPattern, (char*)globalObjectsMask);

	if (gObjectsRef > 0)
	{
		uintptr_t gObjects = *(uintptr_t*)(gObjectsRef + 1);
		GlobalObjects = (TArray<uintptr_t>*)gObjects;
		spdlog::info("Found GlobalObjects by pattern at 0x{:X}", gObjects);
		return gObjects;
	}

	return -1;
}

int UnrealScanner::ValidateGlobalObjectsArray(uintptr_t gObjectsPtr)
{
	// check if gObjectsPtr is a valid pointer
	if (!IsValidPointer(gObjectsPtr))
		return -1;

	uintptr_t* objects = *(uintptr_t**)gObjectsPtr;

	// check if first object pointer is valid
	if (!IsValidObjectPointer(objects[0]))
		return -1;

	int ObjectInternalIntegerOffset = FindObjectInternalIntegerOffset(objects);
	if (ObjectInternalIntegerOffset > 0)
		indexPropOffset = ObjectInternalIntegerOffset;

	return ObjectInternalIntegerOffset;
}
#pragma endregion

#pragma region utilities
bool UnrealScanner::IsValidAddress(uintptr_t address)
{
	MEMORY_BASIC_INFORMATION mem;
	if (VirtualQuery((LPVOID)address, &mem, sizeof(mem)) != sizeof(mem))
		return false;

	if (address > (uintptr_t)mem.BaseAddress + mem.RegionSize)
		return false;

	return !(mem.Protect & PAGE_NOACCESS) && !(mem.Protect & PAGE_GUARD) && mem.State == MEM_COMMIT;
}

bool UnrealScanner::IsValidPointer(uintptr_t pointer)
{
	if (!IsValidAddress(pointer))
		return false;

	uintptr_t address = *(uintptr_t*)pointer;
	if (IsValidAddress(address))
		return true;

	return false;
}

bool UnrealScanner::IsValidObjectPointer(uintptr_t objPtr)
{
	return IsValidPointer(objPtr) && IsValidPointer(*(uintptr_t*)objPtr);
}

int UnrealScanner::FindObjectInternalInteger(int nameIndex, int searchOffset)
{
	static std::unordered_map<int, int> ObjectInternalIntegerCache = {};
	if (ObjectInternalIntegerCache.find(nameIndex) != ObjectInternalIntegerCache.end() && searchOffset < ObjectInternalIntegerCache[nameIndex])
		return ObjectInternalIntegerCache[nameIndex];

	if (GlobalObjects == 0 || nameIndex == -1)
		return -1;

	int ObjectInternalIntegerStart = *(int*)(GlobalObjects->at(0) + indexPropOffset);

	for (int i = searchOffset; i < GlobalObjects->Count(); i++)
	{
		uintptr_t obj = GlobalObjects->at(i);

		if (obj == 0)
			continue;

		if (*(int*)(obj + namePropOffset) == nameIndex)
		{
			ObjectInternalIntegerCache[nameIndex] = i;
			return i;
		}
	}

	return -1;
}

int UnrealScanner::FindObjectInternalInteger(std::string name, int searchOffset)
{
	return FindObjectInternalInteger(FindNameIndex(name), searchOffset);
}

int UnrealScanner::FindNameIndex(std::string search, int searchOffset)
{
	static std::unordered_map<std::string, int> nameIndexCache = {};
	if (nameIndexCache.find(search) != nameIndexCache.end() && searchOffset <= nameIndexCache[search])
		return nameIndexCache[search];

	if (GlobalNames == nullptr)
		return -1;

	for (int i = searchOffset; i < GlobalNames->Count(); i++)
	{
		uintptr_t fNameEntry = GlobalNames->at(i);

		if (fNameEntry == 0)
			continue;

		std::string name = std::string((char*)(fNameEntry + FNameEntryNameOffset));
		if (name == search)
		{
			nameIndexCache[search] = i;
			return i;
		}
	}

	return -1;

}

std::string UnrealScanner::GetObjectName(uintptr_t objectAddr)
{
	int objectNameIndex = *(int*)(objectAddr + namePropOffset);
	uintptr_t objectName = GlobalNames->at(objectNameIndex);
	return std::string((char*)(objectName + FNameEntryNameOffset));
}

std::string UnrealScanner::GetObjectFullName(uintptr_t objectAddr)
{
	std::string objectFullName = GetObjectName(objectAddr);

	for (uintptr_t outer = *(uintptr_t*)(objectAddr + objectOuterOffset); outer > 0; outer = *(uintptr_t*)(outer + objectOuterOffset))
		objectFullName = GetObjectName(outer) + "." + objectFullName;

	return objectFullName;
}

int UnrealScanner::FindFullNameIndex(std::string search, int searchOffset)
{
	static std::unordered_map<std::string, int> fullNameIndexCache = {};
	if (fullNameIndexCache.find(search) != fullNameIndexCache.end())
		return fullNameIndexCache[search];

	for (int i = 0; i < GlobalObjects->Count(); i++)
	{
		auto obj = GlobalObjects->at(i);

		if (obj == 0)
			continue;

		if (GetObjectFullName(obj) == search)
		{
			fullNameIndexCache[search] = i;
			return i;
		}
	}

	return -1;
}

int UnrealScanner::GetStructSize(std::string structName)
{
	int structIndex = FindFullNameIndex(structName);
	uintptr_t str = GlobalObjects->at(structIndex);

	return *(int*)(str + structPropSizeOffset);
}
#pragma endregion