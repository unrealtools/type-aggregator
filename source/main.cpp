#include <TypeAggregator/Types/UnrealType.h>
#include <TypeAggregator/TypeSerializer.h>
#include <TypeAggregator/Types/Core/UScriptStruct.h>
#include <TypeAggregator/Types/GlobalStore.h>
#include <TypeAggregator/TypeDef/Loader.h>
#include <TypeAggregator/UnrealScanner.h>
#include <TypeAggregator/Types/Core/UFunction.h>
#include <TypeAggregator/TypeDef/Builder.h>
#include <TypeAggregator/TypeDef/ClassDef.h>

using namespace TypeAggregator;
using namespace TypeAggregator::Types;
using namespace TypeAggregator::Types::Core;

namespace fs = std::filesystem;

#include <filesystem>
#include <fstream>
#include <spdlog/fmt/fmt.h>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/basic_file_sink.h>

#define NOMINMAX
#include <Windows.h>
#include <TlHelp32.h>


struct TypeAggregatorConfig
{
	uintptr_t GlobalNames = 0x0;
	uintptr_t GlobalObjects = 0x0;
	uint16_t NameEntryOffset = 0x10;
	bool DumpGlobalNames = true;
	bool DumpGlobalObjects = true;
	int LogLevel = spdlog::level::debug;
	std::string ExportDirectory = "./TypeAggregator";
};

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE_WITH_DEFAULT(TypeAggregatorConfig,
	GlobalNames, GlobalObjects, NameEntryOffset, DumpGlobalNames, DumpGlobalObjects, LogLevel, ExportDirectory);

void SuspendThreads(bool suspend = true)
{
	DWORD currProcessId = GetCurrentProcessId();
	DWORD thisThread = GetCurrentThreadId();

	HANDLE hThreadSnap = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, 0);

	THREADENTRY32 thEntry;
	thEntry.dwSize = sizeof(THREADENTRY32);
	if (!Thread32First(hThreadSnap, &thEntry))
		return;

	do
	{
		if (thEntry.th32OwnerProcessID == currProcessId && thEntry.th32ThreadID != thisThread)
		{
			HANDLE hThread = OpenThread(THREAD_ALL_ACCESS, false, thEntry.th32ThreadID);

			if (hThread == INVALID_HANDLE_VALUE)
				continue;

			if (suspend)
				SuspendThread(hThread);
			else
				ResumeThread(hThread);
			CloseHandle(hThread);
		}
	} while (Thread32Next(hThreadSnap, &thEntry));

	CloseHandle(hThreadSnap);
}

void Export()
{
	// read config
	TypeAggregatorConfig config = {};

	try 
	{
		std::ifstream configFile("./TypeAggregator.json");
		if (configFile.good())
		{
			json jconf;
			configFile >> jconf;
			config = jconf;
			configFile.close();
		}
	}
	catch (std::exception& ex)
	{
	}
	

	MessageBoxA(nullptr, "Wait until game is loaded", "WAIT", MB_OK);
	uintptr_t baseAddr = (uintptr_t)GetModuleHandleA(nullptr);

	SuspendThreads();

	fs::path basePath = fs::path(config.ExportDirectory);
	if (!std::filesystem::is_directory(basePath))
		std::filesystem::create_directory(basePath);

	// initialize logger
	spdlog::set_default_logger(spdlog::basic_logger_mt("TypeAggregator", (basePath / "TypeAggregator.log").string(), true));
	spdlog::flush_on(spdlog::level::debug);
	spdlog::set_level(spdlog::level::debug);

	spdlog::info("TypeAggregator is starting...");


	// initialize
	TypeDefLoader* loader = new TypeDefLoader((basePath / "TypeDefs").string());
	TypeRegistry::Initialize(loader);

#ifndef _DEBUG
	try {
#endif
		// find GlobalNames and GlobalObjects array
		UnrealScanner* scanner = new UnrealScanner();
		int gNames = 0;
		int gObjects = 0;

		if (config.GlobalNames == 0)
		{
			spdlog::info("Scanning for GlobalNames with pattern...");
			gNames = scanner->FindGlobalNamesByPattern();

			if (gNames <= 0)
			{
				spdlog::info("GlobalNames pattern scan failed. Trying to bruteforce GlobalNames...");
				gNames = scanner->FindGlobalNames();
			}
		}
		else 
		{
			gNames = baseAddr + config.GlobalNames;
			scanner->GlobalNames = (TArray<uintptr_t>*)(gNames);
			scanner->FNameEntryNameOffset = config.NameEntryOffset;
			spdlog::info("Using configured GlobalNames at 0x{}", gNames);
		}

		if (config.GlobalObjects == 0)
		{
			spdlog::info("Scanning for GlobalObjects with pattern...");
			gObjects = scanner->FindGlobalObjectsByPattern();

			if (gObjects <= 0)
			{
				spdlog::info("GlobalObjects pattern scan failed. Trying to bruteforce GlobalObjects...");
				gObjects = scanner->FindGlobalObjects(gNames);
			}
		}
		else
		{
			gObjects = baseAddr + config.GlobalObjects;
			scanner->GlobalObjects = (TArray<uintptr_t>*)gObjects;
			spdlog::info("Using configured GlobalObjects at 0x{}", gObjects);
		}


		// initialize globals
		GlobalStore::SetNames(gNames);
		GlobalStore::SetObjects(gObjects);

		spdlog::info("Building type definitions...");
		auto builder = new TypeDef::TypeBuilder(scanner);
		builder->BuildAll();
		spdlog::info("TypeDef building finished!");

		if (config.DumpGlobalObjects)
		{
			// save objects array
			std::ofstream objectDump(basePath / "objects.txt");
			for (uint32_t i = 0; i < GlobalStore::Objects()->Count(); i++)
				if (GlobalStore::Objects()->at(i)->GetAddress() != 0)
					objectDump << i << "\t\t" << GlobalStore::Objects()->at(i)->GetFullName() << std::endl;
			objectDump.close();
			spdlog::info("Created object list!");
		}

		if (config.DumpGlobalNames)
		{
			// save name array
			std::ofstream namesDump(basePath / "names.txt");
			for (uint32_t i = 0; i < GlobalStore::Names()->Count(); i++)
				if (GlobalStore::Names()->at(i)->GetAddress() != 0)
					namesDump << i << "\t\t" << GlobalStore::Names()->at(i)->GetName() << std::endl;
			namesDump.close();
			spdlog::info("Created name list!");
		}
#ifndef _DEBUG
	}
	catch (offset_exception& ex)
	{
		spdlog::error(ex.what());
		MessageBoxA(nullptr, "Export failed. See logs for more details!", "ERROR", MB_OK);
		exit(0);
		return;
	}
	catch (std::exception& ex)
	{
		spdlog::error(ex.what());
		MessageBoxA(nullptr, "Export failed. See logs for more details!", "ERROR", MB_OK);
		exit(0);
		return;
	}
#endif

	const std::string exportDir = (basePath / "types").string();
	if (!std::filesystem::is_directory(exportDir))
		std::filesystem::create_directory(exportDir);

	Serializer* s = new Serializer();

	spdlog::info("Start serializing types...");

	std::vector<uintptr_t> generatedObjects = {};

	json j;
	auto objs = GlobalStore::Objects();
	auto objCount = objs->Count();
	for (uint32_t i = 0; i < objCount; i++)
	{
		auto obj = objs->at(i);

		uintptr_t address = obj->GetAddress();
		if (address == 0)
			continue;

		// check if object was already serialized
		if (std::find(generatedObjects.begin(), generatedObjects.end(), address) != generatedObjects.end())
			continue;

		try {
			if (obj->IsA<UScriptStruct>() || obj->IsA<UClass>())
			{
				std::string packageName = obj->GetPackage();
				std::string objectNameCpp = obj->GetNameCPP();

				std::string packageDir = exportDir + "/" + packageName;
				if (!std::filesystem::is_directory(packageDir))
					std::filesystem::create_directory(packageDir);

				std::string fileName = fmt::format("{}/{}/{}.json", exportDir, packageName, objectNameCpp);
				std::ofstream o(fileName);

				if (obj->IsA<UScriptStruct>())
					s->SerializeStruct(j, (UStruct*)obj);
				else
					s->SerializeClass(j, (UClass*)obj);

				o << std::setw(4) << j;
				o.close();
				j.clear();

				generatedObjects.push_back(address);
				spdlog::debug("Serialized [0x{:X}] {}", obj->GetAddress(), objectNameCpp);
			}
		}
		catch (std::exception& e) {
			spdlog::error("Failed to serialize {} because ", obj->GetFullName(), e.what());
		}
	}


	auto typeDefs = TypeRegistry::GetTypeDefs();
	for (auto& strDef : typeDefs)
	{
		if (strDef->Name == "Object")
			continue;

		if (strDef->ObjectInternalInteger > 0 && strDef->ObjectInternalInteger < objCount)
		{
			auto cls = (UClass*)objs->at(strDef->ObjectInternalInteger);
			ClassDef def = cls;
			def.Properties = strDef->Properties;

			std::ofstream out(fmt::format("{}/{}/{}.json", exportDir, def.PackageName, cls->GetNameCPP()));
			out << std::setw(4) << (json)def;
			out.close();
		}
		else 
		{
			json jdef = *strDef;
			jdef["Name"] = (strDef->Name == "Array" || strDef->Name == "Map" ? "T" : "F") + strDef->Name;
			std::ofstream out(fmt::format("{}/{}/{}.json", exportDir, strDef->PackageName, jdef["Name"]));
			out << std::setw(4) << jdef;
			out.close();
		}
	}

	spdlog::info("Finished exporting type infos!");
	MessageBoxA(nullptr, "Export finished!", "Export", MB_OK);
	std::exit(0);
}

static std::thread mainThread;
extern "C" __declspec(dllexport) void InitializeThread()
{
	mainThread = std::thread(Export);
	//Export();
}

bool APIENTRY DllMain(HMODULE, DWORD  ul_reason_for_call, LPVOID)
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		InitializeThread();
		break;
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}