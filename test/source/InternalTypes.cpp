#include "InternalTypes.h"
#include <TypeAggregator/Types/Core/UnrealNames.h>
#include <TypeAggregator/Types/Core/UObject.h>
#include <TypeAggregator/Types/GlobalStore.h>

IUObject* ClassStorage::CorePackage()
{
	IUObject* pkg = nullptr;

	if (!pkg)
	{
		pkg = new IUObject();
		pkg->Name = "Core";
	}

	return pkg;
}

IUClass* ClassStorage::Object()
{
	static IUClass* cls = nullptr;

	if (!cls)
	{
		cls = new IUClass();
		cls->Name = "Object";
		cls->ClassFlags = 200U;
		cls->Class = Class();
		cls->Outer = CorePackage();

		auto pname = new IUProperty();
		pname->Name = "Name";
		pname->Offset = 16;

		auto pindex = new IUProperty();
		pindex->Name = "ObjectIndex";
		pindex->Offset = 8;
		pname->Next = pindex;

		cls->Children = pname;

		cls->ObjectIndex = ClassStorage::IGlobalObjects()->Count();
		ClassStorage::IGlobalObjects()->Push(cls);
	}

	return cls;
}

IUClass* ClassStorage::Class()
{
	IUClass* cls = nullptr;

	if (!cls)
	{
		cls = new IUClass();
		cls->Name = "Class";
		cls->ClassFlags = 4000U;
		cls->SuperField = nullptr;
		cls->Outer = CorePackage();

		auto c = new IUConst();
		c->Name = "Constant";
		c->Value = FString("foo");

		auto e = new IUEnum();
		e->Name = "Enum";
		e->Names.Push("One");
		e->Names.Push("Two");
		e->Names.Push("Three");
		c->Next = e;

		cls->Children = c;

		cls->ObjectIndex = IGlobalObjects()->Count();
		IGlobalObjects()->Push(cls);
	}

	return cls;
}


TypeAggregator::TArray<IFNameEntry*, uint32_t>* ClassStorage::IGlobalNames()
{
	static TypeAggregator::TArray<IFNameEntry*, uint32_t>* gnames = nullptr;

	if (!gnames)
	{
		gnames = new TypeAggregator::TArray<IFNameEntry*, uint32_t>(200);
		TypeAggregator::Types::GlobalStore::SetNames((uintptr_t)gnames);
	}

	return gnames;
}

TypeAggregator::TArray<IUObject*, uint32_t>* ClassStorage::IGlobalObjects()
{
	static TypeAggregator::TArray<IUObject*, uint32_t>* gobjs = nullptr;
	if (!gobjs)
	{
		gobjs = new TypeAggregator::TArray<IUObject*, uint32_t>(200);
		TypeAggregator::Types::GlobalStore::SetObjects((uintptr_t)gobjs);
	}

	return gobjs;
}
