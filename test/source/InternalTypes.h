#pragma once
#include <TypeAggregator/Types/Core/FString.h>
#include <TypeAggregator/Types/Core/ObjectFlags.h>
#include <TypeAggregator/Types/Core/ClassFlags.h>
#include <TypeAggregator/Types/Core/PropertyFlags.h>

#include <cstdint>

#pragma pack(push, 1)
#pragma warning(disable:4996)
using namespace TypeAggregator::Types::Core;

struct IFNameEntry;
class IUObject;
class IUClass;

class ClassStorage
{
public:
	static TypeAggregator::TArray<IFNameEntry*, uint32_t>* IGlobalNames();
	static TypeAggregator::TArray<IUObject*, uint32_t>* IGlobalObjects();

	static IUObject* CorePackage();
	static IUClass* Object();
	static IUClass* Class();
};

struct IFNameEntry
{
	uint64_t dummy;
	uint32_t Index;
	uint32_t dummy2;
	char Name[128];

	IFNameEntry(const char* name)
		: dummy(0), dummy2(0)
	{
		strcpy(this->Name, name);
		Index = ClassStorage::IGlobalNames()->Count();
		ClassStorage::IGlobalNames()->Push(this);
	}
};

struct IFName
{
	uint32_t Index;

	IFName(uint32_t index) : Index(index) {}
	IFName(const char* name)
	{
		for (auto n : *ClassStorage::IGlobalNames())
		{
			if (strcmp(n->Name, name) == 0)
			{
				Index = n->Index;
				return;
			}
		}


		auto entry = new IFNameEntry(name);
		Index = entry->Index;
	}
};


using namespace magic_enum::bitwise_operators;
class IUObject
{
public:
	uint32_t ObjectFlags = (uint32_t)(EObjectFlags::RF_Public | EObjectFlags::RF_Dynamic);
	uint32_t ObjectIndex = 0;
	IUObject* Outer = nullptr;
	IFName Name = "None";
	class IUClass* Class;

	IUObject() = default;

	IUObject(const char* name, class IUClass* cls, uint32_t flags, IUObject* outer = nullptr)
		: ObjectFlags(flags), Outer(outer), Name(name), Class(cls)
	{
	}
};

class IUField : public IUObject 
{
public:
	IUField* Next = nullptr;
};


class IUConst : public IUField 
{
public:
	FString Value = FString();
};


class IUEnum : public IUField 
{
public:
	TypeAggregator::TArray<IFName> Names = TypeAggregator::TArray<IFName>();
};

class IUProperty : public IUField 
{
public:
	uint32_t Offset = 0;
	uint32_t PropertyFlags = (uint32_t)(EPropertyFlags::Native | EPropertyFlags::Transient);
	uint32_t Size = 0;
	uint32_t BitMask = 0;
};

class IUStruct : public IUField 
{
public:
	IUField* SuperField = nullptr;
	IUField* Children = nullptr;
	uint32_t PropertySize = (uint32_t)(EClassFlags::Compiled | EClassFlags::Exported);
};

class IUClass : public IUStruct 
{
public:
	uint32_t ClassFlags = 0;
};
#pragma pack(pop)