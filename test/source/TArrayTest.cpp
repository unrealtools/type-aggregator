#include <doctest/doctest.h>
#include <TypeAggregator/TArray.h>
#include <stdexcept>

using namespace TypeAggregator;

struct Foo
{
    int Bar;
};

Foo foo = { 20 };
Foo* fooRef = &foo;

TArray<int> intArray;
TArray<Foo> fooArray;
TArray<Foo*> fooRefArray;

TEST_CASE("Array initalisation")
{
    intArray = TArray<int>();
    CHECK(intArray.Count() == 0);
    CHECK(intArray.Max() == 0);

    fooArray = {foo};
    CHECK(fooArray.Count() == 1);
    CHECK(fooArray.Max() == 1);
    CHECK(fooArray[0].Bar == foo.Bar);

    fooRefArray = { &foo, &foo };
    CHECK(fooRefArray.Count() == 2);
    CHECK(fooRefArray.Max() == 2);
    CHECK(fooRefArray[0] == &foo);
}

TEST_CASE("Array bounds")
{
    CHECK_THROWS_WITH_AS(intArray[1], "Index out of bounds (1 of 0)", std::out_of_range);
}