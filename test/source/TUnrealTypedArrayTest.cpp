#include <doctest/doctest.h>


#include <TypeAggregator/TArray.h>
#include <TypeAggregator/TUnrealTypeArray.h>
#include "UnrealTypes.h"

using namespace TypeAggregator;

TEST_CASE("TTypedArray")
{
    Bar b = {};
    b.Short = 221;

    StructDef* bDef = new StructDef();
    bDef->Name = "Bar";
    bDef->Size = sizeof(Bar);
    TypeAggregator::Types::TypeRegistry::AddTypeDef<BarType>(bDef);

    TArray<Bar> plainArr = { b, b, b };
    CHECK(plainArr.at(0).Short == b.Short);
    CHECK(plainArr.at(1).Short == b.Short);
    CHECK(plainArr.at(2).Short == b.Short);

    TArray<Bar*> ptrArr = { &b, &b, &b };

    auto typedPlainArr = TUnrealTypeArray<BarType>((uintptr_t)&plainArr);
    CHECK(typedPlainArr.Count() == 3);
    CHECK(typedPlainArr.at(0)->GetShort() == 221);
    CHECK(typedPlainArr[0] == typedPlainArr[0]);
    CHECK(typedPlainArr.at(1)->GetShort() == 221);
    CHECK(typedPlainArr.at(2)->GetShort() == 221);

    auto typedPtrArr = TUnrealTypeArray<BarType*>((uintptr_t)&ptrArr);
    CHECK(typedPtrArr.Count() == 3);
    CHECK(typedPtrArr.at(0)->GetShort() == 221);
    CHECK(typedPtrArr[0] == typedPtrArr[0]);
}