#include <doctest/doctest.h>
#include <TypeAggregator/TypeDef/Loader.h>
#include <TypeAggregator/TypeDef/StructDef.h>
#include <string>
#include <fstream>

const std::string ValidStructDef = R"(
{
	"Name": "FValidStruct",
	"ParentName": "",
	"PackageName": "ValidPackage"
}
)";

TEST_CASE("TypeDefLoader")
{
	using namespace TypeAggregator::TypeDef;

	std::ofstream validStructFile("./FValidStruct.json");
	validStructFile << ValidStructDef;
	validStructFile.close();

	TypeDefLoader typeLoader("./");

	auto def = typeLoader.LoadDefinitionFile("FValidStruct");
	CHECK(def->Name == "FValidStruct");
}