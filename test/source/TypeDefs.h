#pragma once
#include <nlohmann/json.hpp>
#include <TypeAggregator/TypeDef/StructDef.h>
#include "InternalTypes.h"
#include <cstddef>
#include <map>
#include <string>
#include <vector>

using namespace TypeAggregator::TypeDef;

static nlohmann::json FNameDef = {
	{"Name", "Name"},
	{"PackageName", "Core"},
	{"ParentName", ""},
	{"Size", sizeof(IFName)},
	{"Properties", {
		{
			{"Name", "Index"},
			{"Type", "int"},
			{"Offset", offsetof(IFName, Index)}
		}
	}}
};

static nlohmann::json FNameEntryDef = {
	{"Name", "NameEntry"},
	{"PackageName", "Core"},
	{"ParentName",  ""},
	{"Size", sizeof(IFNameEntry)},
	{"Properties", {
		{
			{"Name", "Index"},
			{"Type", "int"},
			{"Offset", offsetof(IFNameEntry, Index)}
		},
		{
			{"Name", "Name"},
			{"Type", "char*"},
			{"Offset", offsetof(IFNameEntry, Name)}
		}
	}}
};

static nlohmann::json UObjectDef = {
	{"Name", "Object"},
	{"PackageName", "Core"},
	{"ParentName", ""},
	{"Size", sizeof(IUObject)},
	{"Properties", {
		{
			{"Name", "ObjectFlags"},
			{"Type", "uint64_t"},
			{"Offset", offsetof(IUObject, ObjectFlags)}
		},
		{
			{"Name", "ObjectIndex"},
			{"Type", "uint32_t"},
			{"Offset", offsetof(IUObject, ObjectIndex)}
		},
		{
			{"Name", "Outer"},
			{"Type", "Core.Object*"},
			{"Offset", offsetof(IUObject, Outer)}
		},
		{
			{"Name", "Name"},
			{"Type", "Core.Name"},
			{"Offset", offsetof(IUObject, Name)}
		},
		{
			{"Name", "Class"},
			{"Type", "Core.Class*"},
			{"Offset", offsetof(IUObject, Class)}
		}
	}}
};

static nlohmann::json UFieldDef = {
	{"Name", "Field"},
	{"PackageName", "Core"},
	{"ParentName", "Core.Object"},
	{"Size", sizeof(IUField)},
	{"Properties", {
		{
			{"Name", "Next"},
			{"Type", "Core.Field*"},
			{"Offset", offsetof(IUField, Next)}
		}
	}}
};

static nlohmann::json UConstDef = {
	{"Name", "Const"},
	{"PackageName", "Core"},
	{"ParentName", "Core.Field"},
	{"Size", sizeof(IUConst)},
	{"Properties", {
		{
			{"Name", "Value"},
			{"Type", "Core.String"},
			{"Offset", offsetof(IUConst, Value)}
		}
	}}
};

static nlohmann::json UEnumDef = {
	{"Name", "Enum"},
	{"PackageName", "Core"},
	{"ParentName", "Core.Field"},
	{"Size", sizeof(IUEnum)},
	{"Properties", {
		{
			{"Name", "Names"},
			{"Type", "Core.Array<Core.Name>"},
			{"Offset", offsetof(IUEnum, Names)}
		}
	}}
};

static nlohmann::json UPropertyDef = {
	{"Name", "Property"},
	{"PackageName", "Core"},
	{"ParentName", "Core.Field"},
	{"Size", sizeof(IUProperty)},
	{"Properties", {
		{
			{"Name", "Offset"},
			{"Type", "uint32_t"},
			{"Offset", offsetof(IUProperty, Offset)}
		},
		{
			{"Name", "PropertyFlags"},
			{"Type", "uint32_t"},
			{"Offset", offsetof(IUProperty, PropertyFlags)}
		},
		{
			{"Name", "Size"},
			{"Type", "uint32_t"},
			{"Offset", offsetof(IUProperty, Size)}
		},
		{
			{"Name", "BitMask"},
			{"Type", "uint32_t"},
			{"Offset", offsetof(IUProperty, BitMask)}
		}
	}}
};

static nlohmann::json UStructDef = {
	{"Name", "Struct"},
	{"PackageName", "Core"},
	{"ParentName", "Core.Field"},
	{"Size", sizeof(IUStruct)},
	{"Properties", {
		{
			{"Name", "SuperField"},
			{"Type", "Core.Field*"},
			{"Offset", offsetof(IUStruct, SuperField)}
		},
		{
			{"Name", "Children"},
			{"Type", "Core.Field*"},
			{"Offset", offsetof(IUStruct, Children)}
		},
		{
			{"Name", "PropertySize"},
			{"Type", "uint32_t"},
			{"Offset", offsetof(IUStruct, PropertySize)}
		}
	}}
};

static nlohmann::json UClassDef = {
	{"Name", "Class"},
	{"PackageName", "Core"},
	{"ParentName", "Core.Struct"},
	{"Size", sizeof(IUClass)},
	{"Properties", {
		{
			{"Name", "ClassFlags"},
			{"Type", "uint32_t"},
			{"Offset", offsetof(IUClass, ClassFlags)}
		}
	}}
};

static std::vector<nlohmann::json> TypeDefs = {
	FNameDef, FNameEntryDef,
	UObjectDef, UFieldDef, UConstDef, UEnumDef, UPropertyDef,
	UStructDef, UClassDef
};

/*static std::map<std::string, nlohmann::json> TypeDefs = {
	{"Core/Name", FNameDef},
	{"Core/NameEntry", FNameEntryDef},
	{"Core/Object", UObjectDef},
	{"Core/Field", UFieldDef},
	{"Core/Const", UConstDef},
	{"Core/Enum", UEnumDef},
	{"Core/Property", UPropertyDef},
	{"Core/Struct", UStructDef},
	{"Core/Class", UClassDef}
};*/