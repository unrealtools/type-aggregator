#include <cstddef>
#include <doctest/doctest.h>
#include <TypeAggregator/Types/Core/UObject.h>
#include <TypeAggregator/Types/Core/UClass.h>
#include <TypeAggregator/Types/Core/UnrealNames.h>
#include "InternalTypes.h"
#include "TypeAggregator/TypeDef/Loader.h"
#include "TypeAggregator/TypeDef/Registry.h"

using namespace TypeAggregator;
using namespace TypeAggregator::Types::Core;

#include <iostream>
TEST_CASE("UnrealObject")
{
	IUObject* iobj = ClassStorage::Object();
	UObject obj = UObject((uintptr_t)iobj);

	CHECK(obj.GetName() == "Object");
	CHECK(obj.GetOuter()->GetAddress() != 0);
	CHECK(obj.GetOuter()->GetName() == "Core");
	CHECK(obj.GetClass()->GetAddress() != 0);
	CHECK(obj.GetClass()->GetName() == "Class");
	CHECK(obj.GetFullName() == "Core.Object");
	CHECK(obj.GetPackage() == "Core");
	CHECK(obj.GetObjectIndex() == 1);
	CHECK(obj.GetObjectFlagsList().size() == 2);

	IUClass* icls = ClassStorage::Class();
	UClass cls = UClass((uintptr_t)icls);
	CHECK(icls->ObjectIndex != 0);
	auto props = cls.GetProperties();
	CHECK(props.size() == 2);
}