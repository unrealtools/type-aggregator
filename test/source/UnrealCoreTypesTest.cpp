#include <doctest/doctest.h>
#include <TypeAggregator/Types/Core/UObject.h>
#include <TypeAggregator/TypeDef/Registry.h>

#include <TypeAggregator/Types/Core/UnrealNames.h>

using namespace TypeAggregator::TypeDef;
using namespace TypeAggregator::Types;

#pragma warning(disable:4996)

struct FName
{
	uint32_t Index;
};

struct FNameEntry
{
	uint64_t dummy;
	uint64_t Index;
	char Name[128];

	FNameEntry(const char* name)
		: dummy(0), Index(0)
	{
		strcpy(this->Name, name);
	}
};

FNameEntry nameEntry0 = FNameEntry("Entry0");
FNameEntry nameEntry1 = FNameEntry("Entry1");

TypeAggregator::TArray<FNameEntry*> GNames = 
{
	&nameEntry0,
	&nameEntry1
};


TEST_CASE("FName")
{
	auto nameTypeDef = TypeRegistry::GetTypeDef<Core::FName>();
	CHECK(nameTypeDef->Properties.size() > 0);
	CHECK(nameTypeDef->HasProperty("Index"));

	FName* name = new FName{ 1 };
	Core::FName nameType = Core::FName((uintptr_t)name);
	CHECK(nameType.GetIndex() == 1);
}


TEST_CASE("FNameEntry")
{
	auto nameEntryTypeDef = TypeRegistry::GetTypeDef<Core::FNameEntry>();
	CHECK(nameEntryTypeDef->Properties.size() > 0);
	CHECK(nameEntryTypeDef->HasProperty("Index"));
	CHECK(nameEntryTypeDef->HasProperty("Name"));

	FNameEntry nameEntry = FNameEntry("TestEntry");
	Core::FNameEntry nameEntryType = Core::FNameEntry((uintptr_t)&nameEntry);
	CHECK(nameEntryType.GetIndex() == 0);
	CHECK(nameEntryType.GetName() == "TestEntry");
}


