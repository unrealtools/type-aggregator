#include <cstdint>
#include <doctest/doctest.h>

#include <nlohmann/json.hpp>
#include "UnrealTypes.h"

using namespace TypeAggregator::Types;


TEST_CASE("UnrealType")
{
    Bar* bar = new Bar();
    Foo* foo = &bar->FooImpl;

    auto barType = BarType((uintptr_t)bar);

    // basic type checks
    CHECK(barType.GetChar() == 0x1);
    CHECK(barType.GetBool() == true);
    CHECK(barType.GetShort() == std::numeric_limits<short>::max());
    CHECK(barType.GetInteger() == std::numeric_limits<int>::max());
    CHECK(barType.GetLong() == std::numeric_limits<long>::max());
    CHECK(barType.GetFloat() == std::numeric_limits<float>::max());
    CHECK(barType.GetEnum() == bar->Enum);
    CHECK(barType.GetIntPtr() == bar->IntPtr);
    CHECK(barType.GetStdString() == bar->StdString);
    CHECK(barType.GetCString() == bar->CString);

    // check impl
    auto fooImplRefType = barType.GetFooImpl();
    CHECK(fooImplRefType.GetAddress() == (uintptr_t)foo);
    CHECK(fooImplRefType.GetA() == foo->A);
    CHECK(fooImplRefType.GetB() == foo->B);
    foo->B = false;
    CHECK(fooImplRefType.GetB() == false);

    // check ref
    auto barRefType = barType.GetBarRef();
    CHECK(barRefType->GetAddress() == (uintptr_t)bar);
    CHECK(barRefType->GetFloat() == bar->Float);
    bar->Float = 0.0f;
    CHECK(barRefType->GetFloat() == 0.0f);
}