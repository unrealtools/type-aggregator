#pragma once

#include <limits>
#include <TypeAggregator/Types/UnrealType.h>

using namespace TypeAggregator::Types;

enum class ENum
{
    A,
    B
};

struct Foo
{
    int A = 2000;
    bool B = true;
};

class FooType : public UnrealType
{
public:
    using UnrealType::UnrealType;

    int GetA() { return Read<int>(0); }
    bool GetB() { return Read<bool>(sizeof(int)); }

    inline static const std::string ClassName = "Core.Foo";
};

struct Bar
{
    char Char = 0x1;
    bool Bool = true;
    short Short = std::numeric_limits<short>::max();
    int Integer = std::numeric_limits<int>::max();
    long Long = std::numeric_limits<long>::max();
    float Float = std::numeric_limits<float>::max();
    char CString[128] = "Hello World";
    int* IntPtr = new int(10);
    std::string StdString = "Hello String";

    // enums
    ENum Enum = ENum::A;

    Foo FooImpl = Foo();
    Bar* BarRef = this;
};

class BarType : public UnrealType
{
public:
    using UnrealType::UnrealType;
    constexpr static const size_t TypeSize = sizeof(Bar);

    char GetChar() { return Read<char>(offsetof(Bar, Char)); }
    bool GetBool() { return Read<bool>(offsetof(Bar, Bool)); }
    short GetShort() { return Read<short>(offsetof(Bar, Short)); }
    int GetInteger() { return Read<int>(offsetof(Bar, Integer)); }
    long GetLong() { return Read<long>(offsetof(Bar, Long)); }
    float GetFloat() { return Read<float>(offsetof(Bar, Float)); }
    int* GetIntPtr() { return Read<int*>(offsetof(Bar, IntPtr)); }
    char* GetCString() { return Read<char*>(offsetof(Bar, CString)); }
    std::string GetStdString() { return Read<std::string>(offsetof(Bar, StdString)); }

    ENum GetEnum() { return Read<ENum>(offsetof(Bar, Enum)); }

    FooType GetFooImpl() { return Read<FooType>(offsetof(Bar, FooImpl)); }
    BarType* GetBarRef() { return Read<BarType*>(offsetof(Bar, BarRef)); }

    inline static const std::string ClassName = "Core.Bar";
};