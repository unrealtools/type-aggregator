//#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#define DOCTEST_CONFIG_IMPLEMENT
//#define DOCTEST_CONFIG_IMPLEMENTATION_IN_DLL
#include <doctest/doctest.h>
#include "TypeDefs.h"
#include <TypeAggregator/Types/Core/UObject.h>
#include <TypeAggregator/Types/Core/UnrealNames.h>
#include <fstream>

int main(int argc, char** argv)
{
	//LoadLibrary("./_deps/typeaggregator-build/Debug/TypeAggregator.dll");

	doctest::Context context;
	context.setOption("order-by", "name");
	context.applyCommandLine(argc, argv);

	if (!std::filesystem::is_directory("./TypeDefs"))
		std::filesystem::create_directory("./TypeDefs");

	for (auto& j : TypeDefs)
	{
		std::string pkgDir = fmt::format("./TypeDefs/{}", j["PackageName"]);
		if (!std::filesystem::is_directory(pkgDir))
			std::filesystem::create_directory(pkgDir);

		std::string path = fmt::format("{}/{}.json", pkgDir, j["Name"]);
		std::ofstream f(path);
		f << std::setw(4) << j << std::endl;
		f.close();
	}

	TypeDefLoader* loader = new TypeDefLoader("./TypeDefs");
	TypeRegistry::Initialize(loader);

	int res = context.run();
	if (context.shouldExit())
		return res;

	return res;
}